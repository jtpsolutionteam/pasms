package br.com.jtpsolution.dao.proposta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_envio_email_taxa", schema = Constants.SCHEMA)
public class TabPropostaEnvioEmailTaxaObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta_envio_email")
	// @NotNull(message = "PropostaEnvioEmail campo obrigatório!")
	private Integer cdPropostaEnvioEmail;

	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "tx_email")
	// @NotEmpty(message = "Email campo obrigatório!")
	@Size(max = 100, message = "Email tamanho máximo de 100 caracteres")
	private String txEmail;

	@Column(name = "dt_envio")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEnvio;

	@Column(name = "ck_enviado")
	// @NotNull(message = "ckEnviado campo obrigatório!")
	private Integer ckEnviado;

	@Column(name = "ck_aberto")
	// @NotNull(message = "ckAberto campo obrigatório!")
	private Integer ckAberto;
	
	@Column(name = "dt_aberto")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAberto;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

}
