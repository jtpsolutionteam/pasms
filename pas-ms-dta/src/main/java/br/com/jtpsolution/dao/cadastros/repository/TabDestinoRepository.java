package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabDestinoObj;



public interface TabDestinoRepository extends JpaRepository<TabDestinoObj, Integer> {



}