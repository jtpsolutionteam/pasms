package br.com.jtpsolution.modulos.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.dao.cadastros.TabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.TabUsuarioObj;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.proposta.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.proposta.TabPropostaEnvioEmailTaxaObj;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.obj.FluxoDeclaracaoEntrada;
import br.com.jtpsolution.modulos.obj.FluxoDeclaracaoTransito;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.email.EmailSend;

@Component
public class RoboDtaService {

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	@Autowired
	private TabPropostaDocumentosRepository tabPropostaDocumentosRepository;

	@Autowired
	private TabPropostaEnvioEmailTaxaService tabPropostaEnvioEmailTaxaService;

	@Autowired
	private EmailSend emailSend;
	
	
	@Scheduled(cron = "0 0/5 8-19 * * ?")
	public void executaRoboAguardandoCarregamento() {
		
		 System.out.println("DTA Robo Aguardando Parametrizacao:" +new Date());
			
		 List<TabPropostaObj> listProposta = tabPropostaRepository.findByDtaAgCarregamentoQuery();
		
		 executaRobo(listProposta);
	}
	
	
	
	@Scheduled(cron = "0 0/20 6-23 * * ?")
	public void executaRoboGeral() {
		
		 System.out.println("DTA Robo Geral - Inicio:" +new Date());
			
		 List<TabPropostaObj> listProposta = tabPropostaRepository.findByConclusaoTransitoNullQuery();
		
		 executaRobo(listProposta);
		 
		 System.out.println("DTA Robo Geral - Final:" +new Date());
	}
	
	
	public void executaRobo(List<TabPropostaObj> listProposta) {

		try {
			
		 // if (GeneralUtil.getProducao()) {
			System.out.println("DTA Robo:" +new Date());
			
			//List<TabPropostaObj> listProposta = tabPropostaRepository.findByConclusaoTransitoNullQuery();
			//List<TabPropostaObj> list = tabPropostaRepository.findByListCdPropostaQuery(22177);
			

			for (TabPropostaObj atualPropostaObj : listProposta) {
				boolean ckFluxo = false;
				boolean ckDesembaraco = false;

				 System.out.println("DTA No: "+atualPropostaObj.getTxNumeroDta());

				if (!Validator.isBlankOrNull(atualPropostaObj.getTxNumeroDta())) {
					
					TabPropostaObj atual = tabPropostaRepository.findByTxNumeroDtaQuery(atualPropostaObj.getTxNumeroDta());

					FluxoDeclaracaoTransito fluxo = consultarDta(GeneralUtil.setTiranaoNumero(atual.getTxNumeroDta()));
					
					System.out.println("Fluxo DTA: "+fluxo);
					
					if (fluxo != null) {

						System.out.println("DTA INICIO: "+atual.getCdProposta() + " - " + atual.getTxNumeroDta());
						
						if (fluxo.getSolicitacaoRegistro() != null) {

							if (fluxo.getSolicitacaoRegistro().getDtSolicitacao() != null) {

								//TabPropostaObj tabProposta = tabPropostaRepository.findByCdPropostaQuery(atual.getCdProposta());

								if (fluxo.getSolicitacaoRegistro() != null) {
									atual.setDtDtaSolicitacao(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getSolicitacaoRegistro().getDtSolicitacao()));
									atual.getTabStatusObj().setCdStatus(5);

									if (fluxo.getSolicitacaoRegistro().getDtRegistro() != null) {
										atual.setDtDtaRegistro(GeneralParser.StringLocalDateTimeToDate(
												fluxo.getSolicitacaoRegistro().getDtRegistro()));
										atual.getTabStatusObj().setCdStatus(6);
									} 

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoSolicitacao())) {
										ckFluxo = true;
										atual.setCkDtaFluxoSolicitacao(1);
									} else {

										TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository
												.findByCdPropostaAndCdClassificacaoQuery(atual.getCdProposta(), 28);
										if (tabP == null) {
											ckFluxo = true;
										}
										TabPropostaDocumentosObj tabD = tabPropostaDocumentosRepository
												.findByCdPropostaAndCdClassificacaoQuery(atual.getCdProposta(), 29);
										if (tabD == null) {
											if (!Validator.isBlankOrNull(atual.getDtDtaDesembaraco())) {
												ckDesembaraco = true;
											}
										}
									}

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoRegistro()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoSolicitacao())) {
										ckFluxo = true;
										atual.setCkDtaFluxoRegistro(1);
									}
								}

								if (fluxo.getSelecaoParametrizada() != null) {
									atual.setDtDtaParametrizacao(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getSelecaoParametrizada().getDtParametrizacao()));
									atual.getTabStatusObj().setCdStatus(7);
									if (fluxo.getSelecaoParametrizada().getTxCanal() != null)
										atual.setTxDtaCanal(fluxo.getSelecaoParametrizada().getTxCanal());

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoParametrizacao()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoRegistro())) {
										ckFluxo = true;
										atual.setCkDtaFluxoParametrizacao(1);
									}

								}

								if (fluxo.getDesembaraco() != null) {
									atual.setDtDtaDesembaraco(GeneralParser
											.StringLocalDateTimeToDate(fluxo.getDesembaraco().getDtDesembaraco()));
									atual.getTabStatusObj().setCdStatus(8);

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoDesembaraco()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoParametrizacao())) {
										ckFluxo = true;
										atual.setCkDtaFluxoDesembaraco(1);
									}

								}

								if (fluxo.getCarregamento() != null) {
									atual.setDtDtaCarregamento(GeneralParser
											.StringLocalDateTimeToDate(fluxo.getCarregamento().getDtCarregamento()));
									atual.getTabStatusObj().setCdStatus(9);

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoCarregamento()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoDesembaraco())) {
										ckFluxo = true;
										atual.setCkDtaFluxoCarregamento(1);
									}

								}

								if (fluxo.getInformacoesVeiculo() != null) {
									if (fluxo.getInformacoesVeiculo().getDtPartidaProcedencia() != null) {
										atual.setDtDtaInicioTransito(GeneralParser.StringLocalDateTimeToDate(
												fluxo.getInformacoesVeiculo().getDtPartidaProcedencia()));
										atual.getTabStatusObj().setCdStatus(10);
									}
									if (Validator.isBlankOrNull(atual.getCkDtaFluxoInicioTransito()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoCarregamento())) {
										ckFluxo = true;
										atual.setCkDtaFluxoInicioTransito(1);
									}
								}

								if (fluxo.getChegadaTransito() != null) {
									atual.setDtDtaChegadaTransito(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getChegadaTransito().getDtChegadaTransito()));
									atual.getTabStatusObj().setCdStatus(11);

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoChegada()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoInicioTransito())) {
										ckFluxo = true;
										atual.setCkDtaFluxoChegada(1);
									}
								}
								
								if (fluxo.getArmazenamento() != null) {
									atual.setDtArmazenagem(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getArmazenamento().getRegistro().getDtArmzenamento()));
									atual.getTabStatusObj().setCdStatus(11);

									if (atual.getDtArmazenagem() == null) {
										ckFluxo = true;
									}
								}

								if (fluxo.getConclusaoTransito() != null) {
									atual.setDtDtaConclusaoTransito(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getConclusaoTransito().getDtConclusaoTransito()));
									atual.getTabStatusObj().setCdStatus(12);

									if (Validator.isBlankOrNull(atual.getCkDtaFluxoConclusaoTransito()) && !Validator.isBlankOrNull(atual.getCkDtaFluxoChegada())) {
										ckFluxo = true;
										atual.setCkDtaFluxoConclusaoTransito(1);
									}								
								}

								
								if (ckFluxo 
										&& Validator.isBlankOrNull(atual.getDtEnvioEmailInformativo())) {

									String txEmail = atual.getTxEmailTaxaInformativo()
											+ ";faturamentodta@band-deicmar.com.br";

									String[] arrEmail = txEmail.split("\\;");

									for (int i = 0; i <= arrEmail.length - 1; i++) {
										if (Validator.isValidEmail(arrEmail[i].trim())) {
											
											String txEmailArray = arrEmail[i].trim();
											
											Map<String, Object> model = new HashMap<String, Object>();
											model.put("cdProposta", atual.getCdProposta());
											model.put("txEmail", txEmailArray);
											model.put("txEmailTaxaInformativo", txEmailArray);
											model.put("txBomDia", GeneralParser.mostraBomDiaTardeNoite());
											model.put("txProposta", atual.getTxProposta());
											model.put("txLote", atual.getTxLote());
											model.put("txBl", atual.getTxBl());
											model.put("txNumeroDta", atual.getTxNumeroDta());
											model.put("vlTaxaInformativo", GeneralParser.doubleToStringMoney(
													atual.getVlTaxaInformativo().doubleValue()));
											model.put("txImportador", atual.getTxImportador() + " - "
													+ atual.getTxCnpjImportador());
											
											Date dtEnvio = new Date();											
											if (emailSend.sendMail("mailinformativotaxa", txEmailArray,
													"Amalog/Bandeirantes - Informativo DTA HUB PORT", model)) {
												atual.setDtEnvioEmailInformativo(new Date());
												System.out.println("Email="+atual.getCdProposta()+"-"+txEmailArray+"-"+1);
												AdicionaEmail(atual.getCdProposta(), txEmailArray, dtEnvio, 1);
											} else {
												AdicionaEmail(atual.getCdProposta(), txEmailArray, dtEnvio, 2);
											}
										}
									}
								}
																
								FluxoDeclaracaoEntrada fluxoEntrada = consultaValoresDta(atual.getTxNumeroDta());
																
								atual.setVlCifOrigem(new BigDecimal(GeneralParser.parseDouble(fluxoEntrada.getVlMercadoriaDolar())));
								atual.setVlCifReal(new BigDecimal(GeneralParser.parseDouble(fluxoEntrada.getVlMercadoriaMoedaNacional())));
								atual.setDtDtaUltimaConsulta(new Date());
								//System.out.println(new Date());
								tabPropostaRepository.save(atual);
								
								System.out.println("DTA FIM: "+atual.getCdProposta() + " - " + atual.getTxNumeroDta());
								
								// Fluxo
								if (ckFluxo) {
									RoboExtratoDta(atual.getCdProposta(), 1, atual.getTxNumeroDta());
									RoboExtratoDta(atual.getCdProposta(), 3, atual.getTxNumeroDta());
								}

								// Desembaraço
								if (ckDesembaraco) {
									RoboExtratoDta(atual.getCdProposta(), 2, atual.getTxNumeroDta());
								}
								
							}
						}
					}
					Thread.sleep(2000);

				}
			}
		  //}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			System.out.println("Erro Robo DTA: "+e.getMessage());
		}

	}

	private void AdicionaEmail(Integer cdProposta, String txEmail, Date dtEnvio, Integer ckEnviado) {

		try {
			TabPropostaEnvioEmailTaxaObj Tab = new TabPropostaEnvioEmailTaxaObj();
			Tab.setCdProposta(cdProposta);
			Tab.setCkEnviado(ckEnviado);
			Tab.setDtEnvio(dtEnvio);
			Tab.setTxEmail(txEmail);

			tabPropostaEnvioEmailTaxaService.gravar(Tab);
		} catch (Exception ex) {

			System.out.println("Erro gravação email log" + ex.getMessage());
		}
	}

	private FluxoDeclaracaoTransito consultarDta(String txNumeroDta) {

		FluxoDeclaracaoTransito jsonObj = null;

		try {
			String json = restconnect
					.enviaRestGet("http://amalog.com.br:8081/transporte/aduaneiro/fluxo/declaracao/" + txNumeroDta);

			ObjectMapper mapper = new ObjectMapper();

			jsonObj = mapper.readValue(json, FluxoDeclaracaoTransito.class);

			return jsonObj;

		} catch (Exception ex) {
			System.out.println("Erro envio fluxo DTA "+txNumeroDta);
		}

		return jsonObj;

	}

	public void RoboExtratoDta(Integer cdProposta, Integer cdTipo, String txNumeroDta) {

		try {
			String txUrl = "http://amalog.com.br:8081/transporte/aduaneiro/extrato/fluxo/"
					+ GeneralUtil.setTiranaoNumero(txNumeroDta);
			String txNomeArquivo = "dtafluxo" + GeneralUtil.setTiranaoNumero(txNumeroDta) + ".pdf";
			Integer cdClassificacao = 28; // Fluxo
			if (cdTipo == 2) { // Desembaraço
				txUrl = "http://amalog.com.br:8081/transporte/aduaneiro/extrato/certificado/desembaraco/"
						+ GeneralUtil.setTiranaoNumero(txNumeroDta);
				cdClassificacao = 29;
				txNomeArquivo = "dtadesembaraco" + GeneralUtil.setTiranaoNumero(txNumeroDta) + ".pdf";
			} else if (cdTipo == 3) { // Declaracao
				txUrl = "http://amalog.com.br:8081/transporte/aduaneiro/extrato/declaracao/"
						+ GeneralUtil.setTiranaoNumero(txNumeroDta);
				cdClassificacao = 34;
				txNomeArquivo = "dtadeclaracao" + GeneralUtil.setTiranaoNumero(txNumeroDta) + ".pdf";
			}

			URL url = new URL(txUrl);
			String pathDir = "/Amalog/files/propostas/" + cdProposta + "/";

			File file = new File(pathDir);

			if (!file.exists()) {
				file.mkdirs();
			}

			file = new File(pathDir + txNomeArquivo);

			if (file.exists()) {
				file.delete();
			}

			InputStream is = url.openStream();
			FileOutputStream fos = new FileOutputStream(file);

			int bytes = 0;

			boolean ckIs = false;

			while ((bytes = is.read()) != -1) {
				fos.write(bytes);
				ckIs = true;
			}

			is.close();

			fos.close();

			if (ckIs) {

				TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository
						.findByCdPropostaAndCdClassificacaoQuery(cdProposta, cdClassificacao);

				TabDocumentosClassificacaoObj tabDoc = new TabDocumentosClassificacaoObj();
				tabDoc.setCdDoctoClassificacao(cdClassificacao);

				TabUsuarioObj tabUsuario = new TabUsuarioObj();
				tabUsuario.setCdUsuario(2);
				
				if (tabP == null) {
										
					TabPropostaDocumentosObj tab = new TabPropostaDocumentosObj();
					tab.setCdProposta(cdProposta);
					tab.setTabDocumentosClassificacaoObj(tabDoc); 
					tab.setTabUsuarioCriacaoObj(tabUsuario);
					tab.setDtCriacao(new Date());
					tab.setCdValidado(1);
					tab.setTxNomeArquivo(txNomeArquivo);
					tab.setTxUrl("/proposta/download/" + cdProposta + "?txNomeArquivo=" + txNomeArquivo);
					tabPropostaDocumentosRepository.save(tab);

				} else {
					tabP.setCdProposta(cdProposta);
					tabP.setTabDocumentosClassificacaoObj(tabDoc); 
					tabP.setTabUsuarioCriacaoObj(tabUsuario);
					tabP.setDtCriacao(new Date());
					tabP.setCdValidado(1);
					tabP.setTxNomeArquivo(txNomeArquivo);
					tabP.setTxUrl("/proposta/download/" + cdProposta + "?txNomeArquivo=" + txNomeArquivo);
					tabPropostaDocumentosRepository.save(tabP);

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());

		}

	}

	
	public FluxoDeclaracaoEntrada consultaValoresDta(String txNumeroDta) {

		FluxoDeclaracaoEntrada jsonObj = null;

		try {
			String json = restconnect
					.enviaRestGet("http://amalog.com.br:8081/transporte/aduaneiro/fluxo/declaracao/entrada/" + GeneralUtil.TiraNaonumero(txNumeroDta));

			ObjectMapper mapper = new ObjectMapper();

			jsonObj = mapper.readValue(json, FluxoDeclaracaoEntrada.class);

			return jsonObj;

		} catch (Exception ex) {

		}

		return jsonObj;

	}
	
	
}
