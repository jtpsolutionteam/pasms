package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;


public class NomeCpf implements Serializable{

	private String txNome;
	private String txCPF;
	public String getTxNome() {
		return txNome;
	}
	public void setTxNome(String txNome) {
		this.txNome = txNome;
	}
	public String getTxCPF() {
		return txCPF;
	}
	public void setTxCPF(String txCPF) {
		this.txCPF = txCPF;
	}
	
	
}