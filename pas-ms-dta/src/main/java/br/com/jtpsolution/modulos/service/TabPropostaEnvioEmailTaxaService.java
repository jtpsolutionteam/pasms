package br.com.jtpsolution.modulos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.proposta.TabPropostaEnvioEmailTaxaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaEnvioEmailTaxaRepository;

@Service
public class TabPropostaEnvioEmailTaxaService {


	
	@Autowired
	private TabPropostaEnvioEmailTaxaRepository  tabRepository;
	



	
	
	public List<TabPropostaEnvioEmailTaxaObj> listar(Integer cdProposta) {
		return tabRepository.findByCdProposta(cdProposta);
		
	}
	


	public TabPropostaEnvioEmailTaxaObj gravar(TabPropostaEnvioEmailTaxaObj Tab) {
		
		TabPropostaEnvioEmailTaxaObj tNovo =  tabRepository.save(Tab);			  
		  
		return tNovo;
		
	}

	public TabPropostaEnvioEmailTaxaObj consultar(Integer CdPropostaEnvioEmail) {
	 
	   TabPropostaEnvioEmailTaxaObj Tab = tabRepository.findByCdPropostaEnvioEmailQuery(CdPropostaEnvioEmail);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdPropostaEnvioEmail) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabPropostaEnvioEmailTaxaObj Tab = tabRepository.findOne(CdPropostaEnvioEmail);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	

	
}
