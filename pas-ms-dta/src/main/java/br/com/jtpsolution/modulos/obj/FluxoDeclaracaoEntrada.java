package br.com.jtpsolution.modulos.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FluxoDeclaracaoEntrada {

	private String txNumeroDta;
	
	private String vlMercadoriaDolar;
	
	private String vlMercadoriaMoedaNacional;
	
	
}
