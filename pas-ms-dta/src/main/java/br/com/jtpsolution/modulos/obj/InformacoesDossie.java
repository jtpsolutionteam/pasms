package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;
import java.util.List;


public class InformacoesDossie implements Serializable{

	
	private String txResultado;
	private List<String> dossiesVinculados;
	private Integer nrDocumentosDisponiveis;
	public String getTxResultado() {
		return txResultado;
	}
	public void setTxResultado(String txResultado) {
		this.txResultado = txResultado;
	}
	public List<String> getDossiesVinculados() {
		return dossiesVinculados;
	}
	public void setDossiesVinculados(List<String> dossiesVinculados) {
		this.dossiesVinculados = dossiesVinculados;
	}
	public Integer getNrDocumentosDisponiveis() {
		return nrDocumentosDisponiveis;
	}
	public void setNrDocumentosDisponiveis(Integer nrDocumentosDisponiveis) {
		this.nrDocumentosDisponiveis = nrDocumentosDisponiveis;
	}

	
	
	
}
