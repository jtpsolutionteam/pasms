package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class Concessao implements Serializable{

	private String dtConcessao;
	private String txInterveniente;
	public String getDtConcessao() {
		return dtConcessao;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	public void setDtConcessao(String dtConcessao) {
		this.dtConcessao = dtConcessao;
	}
	
	
	
	
}
