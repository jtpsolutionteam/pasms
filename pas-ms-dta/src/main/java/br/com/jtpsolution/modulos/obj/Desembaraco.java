package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class Desembaraco implements Serializable{

	
	private String dtDesembaraco;
	private NomeMatricula responsavel;
	private String txInterveniente;
	
	
	public NomeMatricula getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(NomeMatricula responsavel) {
		this.responsavel = responsavel;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	public String getDtDesembaraco() {
		return dtDesembaraco;
	}
	public void setDtDesembaraco(String dtDesembaraco) {
		this.dtDesembaraco = dtDesembaraco;
	}
	
	
	
}
