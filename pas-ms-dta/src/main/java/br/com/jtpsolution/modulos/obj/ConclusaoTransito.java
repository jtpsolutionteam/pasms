package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class ConclusaoTransito implements Serializable{


	private String txResultado;
	private String dtConclusaoTransito;
	private String txInterveniente;
	public String getTxResultado() {
		return txResultado;
	}
	public void setTxResultado(String txResultado) {
		this.txResultado = txResultado;
	}
	public String getDtConclusaoTransito() {
		return dtConclusaoTransito;
	}
	public void setDtConclusaoTransito(String dtConclusaoTransito) {
		this.dtConclusaoTransito = dtConclusaoTransito;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	
	
}
