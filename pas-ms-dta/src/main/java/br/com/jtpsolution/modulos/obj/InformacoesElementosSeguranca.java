package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class InformacoesElementosSeguranca implements Serializable{

	
	private String dtInformacoes;
	private NomeMatricula responsavel;
	private String txInterveniente;
	
	public String getDtInformacoes() {
		return dtInformacoes;
	}
	public void setDtInformacoes(String dtInformacoes) {
		this.dtInformacoes = dtInformacoes;
	}
	public NomeMatricula getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(NomeMatricula responsavel) {
		this.responsavel = responsavel;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	
	
}
