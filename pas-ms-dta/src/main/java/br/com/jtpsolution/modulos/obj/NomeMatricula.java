package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;


public class NomeMatricula implements Serializable{

	private String txMatricula;
	private String txNome;
	public String getTxMatricula() {
		return txMatricula;
	}
	public void setTxMatricula(String txMatricula) {
		this.txMatricula = txMatricula;
	}
	public String getTxNome() {
		return txNome;
	}
	public void setTxNome(String txNome) {
		this.txNome = txNome;
	}
	
	
}