package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;


public class UnidadeRecinto implements Serializable{

	
	private UnidadeLocal unidadeLocal;
	private RecintoAduaneiro recintoAduaneiro;
	public UnidadeLocal getUnidadeLocal() {
		return unidadeLocal;
	}
	public void setUnidadeLocal(UnidadeLocal unidadeLocal) {
		this.unidadeLocal = unidadeLocal;
	}
	public RecintoAduaneiro getRecintoAduaneiro() {
		return recintoAduaneiro;
	}
	public void setRecintoAduaneiro(RecintoAduaneiro recintoAduaneiro) {
		this.recintoAduaneiro = recintoAduaneiro;
	}
	
	
}
