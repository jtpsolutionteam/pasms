package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;


public class NomeCnpj implements Serializable{
	private String txCNPJ;
	private String txNome;
	
	public String getTxCNPJ() {
		return txCNPJ;
	}
	public void setTxCNPJ(String txCNPJ) {
		this.txCNPJ = txCNPJ;
	}
	public String getTxNome() {
		return txNome;
	}
	public void setTxNome(String txNome) {
		this.txNome = txNome;
	}
	
	
}