package br.com.jtpsolution.modulos.obj;

import java.io.Serializable;


public class Armazenamento implements Serializable{
	
	private ArmazenamentoComum registro;
	private ArmazenamentoComum encerramento;
	
	public ArmazenamentoComum getRegistro() {
		return registro;
	}
	public void setRegistro(ArmazenamentoComum registro) {
		this.registro = registro;
	}
	public ArmazenamentoComum getEncerramento() {
		return encerramento;
	}
	public void setEncerramento(ArmazenamentoComum encerramento) {
		this.encerramento = encerramento;
	}
	
	
	
}
