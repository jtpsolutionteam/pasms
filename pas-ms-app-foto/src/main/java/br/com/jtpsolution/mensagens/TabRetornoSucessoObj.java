package br.com.jtpsolution.mensagens;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabRetornoSucessoObj {

	
	private String tx_chave;
	
	private Integer cd_mensagem;
	
	private String tx_mensagem;
	
	
	private Long dt_system;

	
	
}
