package br.com.jtpsolution.util.email;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailSend {

	@Autowired
	private EmailService emailService;

	public boolean sendMail(String template, String to, String subject, Map<String, Object> model) {

		boolean ckOk = false;
		try {
			Mail mail = new Mail();
			mail.setFrom("no-reply@amalog.com.br");
			mail.setTo(to);
			mail.setSubject(subject);

			mail.setModel(model);

			emailService.sendSimpleMessage(mail, template);
			ckOk = true;

		} catch (Exception ex) {
			//ex.printStackTrace();
			ckOk = false;
			System.out.println("Erro envio email: "+ex.getMessage());
		}

		return ckOk;
	}

}
