package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabPerfilVeiculoObj;



public interface TabPerfilVeiculoRepository extends JpaRepository<TabPerfilVeiculoObj, Integer> {



}