package br.com.jtpsolution.dao.proposta;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.TabTipoCarregamentoObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabPropostaObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta")
//@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@ManyToOne
	@JoinColumn(name = "cd_cliente")
//@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;

	@JsonFormat(timezone = "UTC-3", pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "dt_proposta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtProposta;

	@Column(name = "tx_proposta")
//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres")
	private String txProposta;

	@ManyToOne
	@JoinColumn(name = "cd_status")
//@NotNull(message = "Status campo obrigatório!")
	private TabStatusObj tabStatusObj;

	@ManyToOne
	@JoinColumn(name = "cd_tipo_carregamento")
//@NotNull(message = "TipoCarregamento campo obrigatório!")
	private TabTipoCarregamentoObj  tabTipoCarregamentoObj;

	@ManyToOne
	@JoinColumn(name = "cd_destino")
//@NotNull(message = "Destino campo obrigatório!")
	private TabDestinoObj tabDestinoObj;

	@Column(name = "tx_lote")
//@NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 20, message = "Lote tamanho máximo de 20 caracteres")
	private String txLote;

	@Column(name = "tx_importador")
//@NotEmpty(message = "Importador campo obrigatório!")
	@Size(max = 100, message = "Importador tamanho máximo de 100 caracteres")
	private String txImportador;

	@Column(name = "tx_cnpj_importador")
//@NotEmpty(message = "CnpjImportador campo obrigatório!")
	@Size(max = 200, message = "CnpjImportador tamanho máximo de 200 caracteres")
	private String txCnpjImportador;

	@Column(name = "tx_bl")
//@NotEmpty(message = "Bl campo obrigatório!")
	@Size(max = 30, message = "Bl tamanho máximo de 30 caracteres")
	private String txBl;
	
	@Column(name = "tx_container")
	private String txContainer;
	
	
	@OneToMany(mappedBy = "cdProposta", targetEntity = TabPropostaDocumentosObj.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Where(clause = "cd_classificacao = 35")
	List<TabPropostaDocumentosObj> listPropostasDoctos;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {

	}

}
