package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabMotoristaObj;



public interface TabMotoristaRepository extends JpaRepository<TabMotoristaObj, Integer> {



}