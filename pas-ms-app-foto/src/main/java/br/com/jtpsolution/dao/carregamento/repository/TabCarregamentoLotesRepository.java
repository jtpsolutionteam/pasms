package br.com.jtpsolution.dao.carregamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.carregamento.TabCarregamentoLotesObj;



public interface TabCarregamentoLotesRepository extends JpaRepository<TabCarregamentoLotesObj, Integer> {



}