package br.com.jtpsolution.dao.cadastros;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_veiculo", schema = Constants.SCHEMA)
public class TabVeiculoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_veiculo")
//@NotNull(message = "Veiculo campo obrigatório!")
	private Integer cdVeiculo;

	@Column(name = "tx_veiculo")
//@NotEmpty(message = "Veiculo campo obrigatório!")
	@Size(max = 100, message = "Veiculo tamanho máximo de 100 caracteres")
	private String txVeiculo;

	@Column(name = "tx_placa")
//@NotEmpty(message = "Placa campo obrigatório!")
	@Size(max = 12, message = "Placa tamanho máximo de 12 caracteres")
	private String txPlaca;

	@JsonIgnore
	@Column(name = "ck_ativo")
//@NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txVeiculo))
			txVeiculo = txVeiculo.toUpperCase();
		if (!Validator.isBlankOrNull(txPlaca))
			txPlaca = txPlaca.toUpperCase();
	}

}
