package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.proposta.TabPropostaObj;



public interface TabPropostaRepository extends JpaRepository<TabPropostaObj, Integer> {



	
	List<TabPropostaObj> findByTxBl(String txBl);
	
	List<TabPropostaObj> findByTxLote(String txLote);
	


	
}