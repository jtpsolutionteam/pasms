package br.com.jtpsolution.dao.carregamento.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.carregamento.TabCarregamentoObj;



public interface TabCarregamentoRepository extends JpaRepository<TabCarregamentoObj, Integer> {

	@Query("select t from TabCarregamentoObj t where t.cdCarregamento = ?1")
	TabCarregamentoObj findByCdCarregamentoQuery(Integer cdCarregamento);

	@Query("select t from TabCarregamentoObj t where t.dtCarregamento is null")
	List<TabCarregamentoObj> findByListaCarregamentoQuery();
	
	
}