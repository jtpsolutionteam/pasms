package br.com.jtpsolution.dao.cadastros;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_perfil_veiculo", schema = Constants.SCHEMA)
public class TabPerfilVeiculoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_perfil_veiculo")
	// @NotNull(message = "PerfilVeiculo campo obrigatório!")
	private Integer cdPerfilVeiculo;

	@Column(name = "tx_perfil_veiculo")
	// @NotEmpty(message = "PerfilVeiculo campo obrigatório!")
	@Size(max = 45, message = "PerfilVeiculo tamanho máximo de 45 caracteres")
	private String txPerfilVeiculo;
	
	@Column(name = "vl_peso")
	//@NotNull(message = "Peso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlPeso;

	@Column(name = "vl_m3")
	//@NotNull(message = "M3 campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlM3;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txPerfilVeiculo))
			txPerfilVeiculo = txPerfilVeiculo.toUpperCase();
	}

	

}
