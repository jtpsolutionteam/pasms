package br.com.jtpsolution.dao.proposta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_documentos", schema = Constants.SCHEMA)
public class TabPropostaDocumentosObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta_documento")
	private Integer cdPropostaDocumento;

	@Column(name = "cd_proposta")
	private Integer cdProposta;

	@Column(name = "tx_nome_arquivo")
	private String txNomeArquivo;

	@Column(name = "tx_url")
	private String txUrl;
	
	@Column(name = "cd_classificacao")
	private Integer cdClassificacao;
	
	@JsonFormat(timezone = "UTC-3", pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCriacao;
	
	@Column(name = "cd_usuario_criacao")
	private Integer cdUsuarioCriacao;
	
	@JsonIgnore
	@Column(name = "tx_imagem_base64")
	private String txImagemBase64;
	
	
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}



}
