package br.com.jtpsolution.dao.carregamento;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabUsuarioObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabCarregamentoLotesObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "cd_carregamento") 
	private Integer cdCarregamento;
	
	@Column(name = "tx_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private String txProposta;
	
	@ManyToOne
	@JoinColumn(name = "cd_usuario")
	// @NotNull(message = "Usuario campo obrigatório!")
	private TabUsuarioObj tabUsuarioObj;

	@ManyToOne
	@JoinColumn(name = "cd_cliente")
	@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;
	
	@Column(name = "tx_lote")	
	private String txLote;
		
	@Column(name = "vl_altura")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAltura;

	@Column(name = "vl_largura")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlLargura;
	
	@Column(name = "vl_peso_extrato_desova")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlPesoExtratoDesova;
	
	@Column(name = "vl_peso_bruto")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlPesoBruto;

	@Column(name = "vl_m3_pack")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlM3Pack;

	
	@Column(name = "vl_comprimento")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlComprimento;
	
	@Column(name = "vl_qtde_volume") 
	private Integer vlQtdeVolume;

	@Column(name = "tx_volume") 
	private String txVolume;
	
	@Column(name = "vl_cif_real")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlCifReal;
	


	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {

	}
	
	@PostLoad
	private void postLoad() {
		
	}

}
