package br.com.jtpsolution.dao.cadastros;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_usuario", schema = Constants.SCHEMA)
public class TabUsuarioObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_usuario")
	// @NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@JsonIgnore
	@Column(name = "ck_ativo")
	private Integer ckAtivo;

	@JsonIgnore
	@Column(name = "tx_nome")
	//@NotEmpty(message = "Nome campo obrigatório!")
	@Size(max = 200, message = "Nome tamanho máximo de 200 caracteres")
	private String txNome;

	@Column(name = "tx_apelido")
	@NotEmpty(message = "Apelido campo obrigatório!")
	@Size(max = 200, message = "Apelido tamanho máximo de 200 caracteres")
	private String txApelido;

	@JsonIgnore
	@Column(name = "tx_senha")
	@NotEmpty(message = "Senha campo obrigatório!")
	@Size(max = 200, message = "Senha tamanho máximo de 200 caracteres")
	private String txSenha;

	@JsonIgnore
	@Column(name = "dt_validade_senha")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtValidadeSenha;
	
	@JsonIgnore
	@Column(name = "tx_email")
	@NotEmpty(message = "Email campo obrigatório!")
	@Size(max = 100, message = "Email tamanho máximo de 100 caracteres")
	private String txEmail;

	@JsonIgnore
	@Column(name = "tx_tel")
	// @NotEmpty(message = "Tel campo obrigatório!")
	@Size(max = 20, message = "Tel tamanho máximo de 20 caracteres")
	private String txTel;

	@JsonIgnore
	@Column(name = "cd_grupo_visao")
	private Integer cdGrupoVisao;

	@JsonIgnore
	@Column(name = "cd_grupo_acesso")
	private Integer cdGrupoAcesso;
	
	@JsonIgnore
	@Column(name = "cd_idioma")
	@NotEmpty(message = "Idioma campo obrigatório!")
	private String cdIdioma;
	
	@JsonIgnore
	@Column(name = "cd_empresa")
	private Integer cdEmpresa;
	
	@JsonIgnore
	@Transient
	private String txConfSenha;

	@JsonIgnore
	@Transient
	private String txConfEmail;
	
	@JsonIgnore
	@Transient
	private String txCnpj;

	
	

	
	
}
