package br.com.jtpsolution.dao.cadastros;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_motorista", schema = Constants.SCHEMA)
public class TabMotoristaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_motorista")
//@NotNull(message = "Motorista campo obrigatório!")
	private Integer cdMotorista;

	@Column(name = "tx_motorista")
//@NotEmpty(message = "Motorista campo obrigatório!")
	@Size(max = 100, message = "Motorista tamanho máximo de 100 caracteres")
	private String txMotorista;

	@Column(name = "tx_cpf")
//@NotEmpty(message = "Cpf campo obrigatório!")
	@Size(max = 20, message = "Cpf tamanho máximo de 20 caracteres")
	private String txCpf;

	@Column(name = "tx_cnh")
//@NotEmpty(message = "Cnh campo obrigatório!")
	@Size(max = 10, message = "Cnh tamanho máximo de 10 caracteres")
	private String txCnh;

	@JsonIgnore
	@Column(name = "ck_ativo")
//@NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txMotorista))
			txMotorista = txMotorista.toUpperCase();
		if (!Validator.isBlankOrNull(txCpf))
			txCpf = txCpf.toUpperCase();
		if (!Validator.isBlankOrNull(txCnh))
			txCnh = txCnh.toUpperCase();
	}

}
