package br.com.jtpsolution.dao.carregamento;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabMotoristaObj;
import br.com.jtpsolution.dao.cadastros.TabStatusCarregamentoObj;
import br.com.jtpsolution.dao.cadastros.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.TabVeiculoObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_carregamento", schema = Constants.SCHEMA)
public class TabCarregamentoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_carregamento")
//@NotNull(message = "Carregamento campo obrigatório!")
	private Integer cdCarregamento;

	@JsonFormat(timezone = "UTC-3", pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCriacao;
	
	@ManyToOne
	@JoinColumn(name = "cd_usuario_criacao")
//@NotNull(message = "UsuarioCriacao campo obrigatório!")
	private TabUsuarioObj tabUsuarioCriacaoObj;

	@JsonFormat(timezone = "UTC-3", pattern = "dd/MM/yyyy")
	@Column(name = "dt_previsao_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrevisaoCarregamento;
	
	@JsonFormat(timezone = "UTC-3", pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "dt_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCarregamento;

	@ManyToOne
	@JoinColumn(name = "cd_veiculo")
//@NotNull(message = "Veiculo campo obrigatório!")
	private TabVeiculoObj tabVeiculoObj;

	@ManyToOne
	@JoinColumn(name = "cd_motorista")
//@NotNull(message = "Motorista campo obrigatório!")
	private TabMotoristaObj tabMotoristaObj;

	@ManyToOne
	@JoinColumn(name = "cd_status")
//@NotNull(message = "Status campo obrigatório!")
	private TabStatusCarregamentoObj tabStatusCarregamentoObj;
	
	@Column(name = "vl_total_m3")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTotalM3;
	
	@Column(name = "vl_total_peso")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTotalPeso;

	@Column(name = "vl_total_cif_real")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTotalCifReal;

	
	@Transient
	private String txLote;
	
	@OneToMany(mappedBy = "cdCarregamento", targetEntity = TabCarregamentoLotesObj.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	List<TabCarregamentoLotesObj> listCarregamentoLotes;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
