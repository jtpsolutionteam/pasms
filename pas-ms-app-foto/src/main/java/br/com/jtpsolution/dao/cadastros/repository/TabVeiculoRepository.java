package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabVeiculoObj;



public interface TabVeiculoRepository extends JpaRepository<TabVeiculoObj, Integer> {



}