package br.com.jtpsolution.modulos.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.carregamento.TabCarregamentoObj;
import br.com.jtpsolution.dao.carregamento.repository.TabCarregamentoRepository;
import br.com.jtpsolution.dao.proposta.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaRepository;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.obj.TabPropostaLoteFotosObj;
import br.com.jtpsolution.modulos.obj.TabPropostaLoteObj;
import br.com.jtpsolution.modulos.obj.TabUploadDoctosObj;
import br.com.jtpsolution.util.Validator;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/proposta")
public class TabPropostaController {

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	@Autowired
	private TabPropostaDocumentosRepository tabPropostaDocumentosRepository;
	
	@Autowired
	private TabCarregamentoRepository tabCarregamentoRepository;

	@GetMapping("/{txBl}")
	public @ResponseBody List<TabPropostaObj> listarProposta(@PathVariable String txBl) {

		List<TabPropostaObj> lst = tabPropostaRepository.findByTxBl(txBl);

		return lst;

	}
	
	@GetMapping("/lote/{txLote}")
	public @ResponseBody List<TabPropostaObj> listarPropostaLote(@PathVariable String txLote) {

		List<TabPropostaObj> lst = tabPropostaRepository.findByTxLote(txLote);

		return lst;

	}
	
	@PostMapping("/gravardoctos")
	public @ResponseBody TabRetornoSucessoObj gravardoctos(@RequestBody TabUploadDoctosObj tabUploadDoctosObj) {

		TabRetornoSucessoObj txRetorno = new TabRetornoSucessoObj();

		if (Validator.isBlankOrNull(tabUploadDoctosObj.getCdProposta())) {
			txRetorno.setTx_chave(String.valueOf(tabUploadDoctosObj.getCdProposta()));
			txRetorno.setCd_mensagem(2);
			txRetorno.setTx_mensagem("Proposta Inválida!");
			txRetorno.setDt_system(new Date().getTime());

		} else {

			System.out.println("Proposta: " + tabUploadDoctosObj.getCdProposta());

				String txNomeArquivo = tabUploadDoctosObj.getTxNomeArquivo();
				
				if (convertBase64(tabUploadDoctosObj.getCdProposta(), txNomeArquivo, tabUploadDoctosObj.getTxArquivo())) {

						TabPropostaDocumentosObj Tab = new TabPropostaDocumentosObj();
						Tab.setCdProposta(tabUploadDoctosObj.getCdProposta());
						Tab.setCdClassificacao(tabUploadDoctosObj.getCdClassificacao());
						Tab.setCdUsuarioCriacao(2);
						Tab.setDtCriacao(new Date());
						Tab.setTxImagemBase64(tabUploadDoctosObj.getTxArquivo());
						Tab.setTxUrl("/proposta/download/" + tabUploadDoctosObj.getCdProposta() + "?txNomeArquivo="
								+ txNomeArquivo);
						Tab.setTxNomeArquivo(txNomeArquivo);
						tabPropostaDocumentosRepository.save(Tab);

						System.out.println("Arquivo: " + txNomeArquivo);
					}
				}

			

			txRetorno.setTx_chave(String.valueOf(tabUploadDoctosObj.getCdProposta()));
			txRetorno.setCd_mensagem(1);
			txRetorno.setTx_mensagem("Arquivos recebidos com sucesso!");
			txRetorno.setDt_system(new Date().getTime());
		

		return txRetorno;

	}


	@PostMapping("/gravarfotosdesova")
	public @ResponseBody TabRetornoSucessoObj gravarFotosDesova(@RequestBody TabPropostaLoteObj tabPropostaLoteObj) {

		TabRetornoSucessoObj txRetorno = new TabRetornoSucessoObj();

		if (Validator.isBlankOrNull(tabPropostaLoteObj.getCdProposta())) {
			txRetorno.setTx_chave(String.valueOf(tabPropostaLoteObj.getCdProposta()));
			txRetorno.setCd_mensagem(2);
			txRetorno.setTx_mensagem("Proposta Inválida!");
			txRetorno.setDt_system(new Date().getTime());

		} else {

			System.out.println("Proposta: " + tabPropostaLoteObj.getCdProposta());

			Integer cdFoto = 0;
			for (TabPropostaLoteFotosObj atual : tabPropostaLoteObj.getListFotos()) {

				cdFoto++;
				String txNomeArquivo = "FotoDesova" + new Date().getTime() + ".jpg";
				
				if (tabPropostaLoteObj.getCdProposta() != 10365) {
					if (convertBase64(tabPropostaLoteObj.getCdProposta(), txNomeArquivo, atual.getTxArquivo())) {

						TabPropostaDocumentosObj Tab = new TabPropostaDocumentosObj();
						Tab.setCdProposta(tabPropostaLoteObj.getCdProposta());
						Tab.setCdClassificacao(35);
						Tab.setCdUsuarioCriacao(tabPropostaLoteObj.getCdUsuario());
						Tab.setDtCriacao(new Date());
						Tab.setTxImagemBase64(atual.getTxArquivo());
						Tab.setTxUrl("/proposta/download/" + tabPropostaLoteObj.getCdProposta() + "?txNomeArquivo="
								+ txNomeArquivo);
						Tab.setTxNomeArquivo(txNomeArquivo);
						tabPropostaDocumentosRepository.save(Tab);

						System.out.println("Arquivo: " + atual.getTxArquivo());
					}
				}

			}

			txRetorno.setTx_chave(String.valueOf(tabPropostaLoteObj.getCdProposta()));
			txRetorno.setCd_mensagem(1);
			txRetorno.setTx_mensagem("Fotos recebidas com sucesso!");
			txRetorno.setDt_system(new Date().getTime());
		}

		return txRetorno;

	}

	@GetMapping("/consultacarregamento/{cdCarregamento}")
	public @ResponseBody TabCarregamentoObj consultacarregamento(@PathVariable Integer cdCarregamento) {

		TabCarregamentoObj tab = tabCarregamentoRepository.findByCdCarregamentoQuery(cdCarregamento);

		return tab;

	}
	
	@GetMapping("/listarcarregamento")
	public @ResponseBody List<TabCarregamentoObj> listarPropostaCarregamento() {

		List<TabCarregamentoObj> lst = tabCarregamentoRepository.findByListaCarregamentoQuery();

		return lst;

	}
	
	@PostMapping("/gravarfotoscarregamento")
	public @ResponseBody TabRetornoSucessoObj gravarFotosCarregamento(@RequestBody TabPropostaLoteObj tabPropostaLoteObj) {

		TabRetornoSucessoObj txRetorno = new TabRetornoSucessoObj();

		if (Validator.isBlankOrNull(tabPropostaLoteObj.getCdProposta())) {
			txRetorno.setTx_chave(String.valueOf(tabPropostaLoteObj.getCdProposta()));
			txRetorno.setCd_mensagem(2);
			txRetorno.setTx_mensagem("Proposta Inválida!");
			txRetorno.setDt_system(new Date().getTime());

		} else {

			System.out.println("Proposta: " + tabPropostaLoteObj.getCdProposta());

			Integer cdFoto = 0;
			for (TabPropostaLoteFotosObj atual : tabPropostaLoteObj.getListFotos()) {

				cdFoto++;
				String txNomeArquivo = "FotoCarregamento" + new Date().getTime() + ".jpg";
				
				if (tabPropostaLoteObj.getCdProposta() != 10365) {
					if (convertBase64(tabPropostaLoteObj.getCdProposta(), txNomeArquivo, atual.getTxArquivo())) {

						TabPropostaDocumentosObj Tab = new TabPropostaDocumentosObj();
						Tab.setCdProposta(tabPropostaLoteObj.getCdProposta());
						Tab.setCdClassificacao(55);
						Tab.setCdUsuarioCriacao(tabPropostaLoteObj.getCdUsuario());
						Tab.setDtCriacao(new Date());
						Tab.setTxImagemBase64(atual.getTxArquivo());
						Tab.setTxUrl("/proposta/download/" + tabPropostaLoteObj.getCdProposta() + "?txNomeArquivo="
								+ txNomeArquivo);
						Tab.setTxNomeArquivo(txNomeArquivo);
						tabPropostaDocumentosRepository.save(Tab);

						System.out.println("Arquivo: " + atual.getTxArquivo());
					}
				}

			}

			txRetorno.setTx_chave(String.valueOf(tabPropostaLoteObj.getCdProposta()));
			txRetorno.setCd_mensagem(1);
			txRetorno.setTx_mensagem("Fotos recebidas com sucesso!");
			txRetorno.setDt_system(new Date().getTime());
		}

		return txRetorno;

	}

	
	
	private boolean convertBase64(Integer cdProposta, String txNomeArquivo, String imgBase64) {

		try {

			byte dearr[] = Base64.getDecoder().decode(imgBase64);
			FileOutputStream fos = new FileOutputStream(
					new File("/Amalog/files/propostas/" + cdProposta + "/" + txNomeArquivo));
			fos.write(dearr);
			fos.close();

			return true;

		} catch (Exception e) {

			// TODO: handle exception
			System.out.println("Erro base64");
			return false;
			// e.printStackTrace();
		}

	}

	
	@RequestMapping(value = "/download/{cdProposta}")
	public @ResponseBody HttpEntity<InputStreamResource> getFile(@PathVariable Integer cdProposta,
			@RequestParam String txNomeArquivo) {

		String pathDir = "/Amalog/files/propostas/" + cdProposta + "/";
		MediaType m = null;
		File destinationFile = new File(pathDir + txNomeArquivo);
		InputStreamResource resource = null;
		try {

			resource = new InputStreamResource(new FileInputStream(destinationFile));

			if (txNomeArquivo.toLowerCase().contains(".jpg")) {
				m = MediaType.IMAGE_JPEG;
			} else if (txNomeArquivo.toLowerCase().contains(".gif")) {
				m = MediaType.IMAGE_GIF;
			} else if (txNomeArquivo.toLowerCase().contains(".png")) {
				m = MediaType.IMAGE_PNG;
			} else if (txNomeArquivo.toLowerCase().contains(".pdf")) {
				m = MediaType.APPLICATION_PDF;
			} else {
				m = MediaType.APPLICATION_OCTET_STREAM;
			}

		} catch (Exception ex) {
			System.out.println("Erro Download Arquivo: " + pathDir + txNomeArquivo);
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + txNomeArquivo)
				.contentType(m).contentLength(destinationFile.length()).body(resource);
	}
	
}
