package br.com.jtpsolution.modulos.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabUploadDoctosObj {

	private Integer cdProposta;
	
	private Integer cdClassificacao;
	
	private String txArquivo;
	
	
	private String txNomeArquivo;
	
	
	
}
