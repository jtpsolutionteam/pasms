package br.com.jtpsolution.modulos.obj;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabPropostaLoteObj {
	
	private Integer cdProposta;
	
	private Integer cdUsuario;

	List<TabPropostaLoteFotosObj> listFotos;
	
	
}
