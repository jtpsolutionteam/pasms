package br.com.jtpsolution;

public class Constants {

	
		public static final String URL_MARIMEX_TESTE = "http://wservices_hml.marimex.com.br";
		public static final String URL_MARIMEX_PRODUCAO = "http://wservices_hml.marimex.com.br";
	
	
		public static final String SCHEMA = "amalog";	
	
		// Constantes relativas as Mensagens de Retorno	
		public static final String REGISTRO_INCLUIDO_ALTERADO_SUCESSO = "Registro Incluído/Alterado com sucesso!";	
		public static final String LOGIN_OK = "Login efetuado com sucesso!";
		public static final String REGISTRO_EXCLUIDO_SUCESSO = "Registro excluído com sucesso!";	
		public static final String ERRO_AO_EXCLUIR = "Erro ao excluir o registro!";
		
		public static final String REGISTRO_NAO_EXISTE = "Registro não existe!";
		public static final String FILTRO_NAO_EXISTE = "Não existem dados para o filtro desejado!";
		public static final String HASH_INVALIDO = "Hash de segurança inválido!";
		public static final String ERRO_UPLOAD = "Erro no envio do Arquivo!";
		public static final String ERRO_UPLOAD_ARQUIVO_JA_EXISTE = "Arquivo já existe!";

		
		
		public static final String NOME_SISTEMA_LOCAL = "amalog";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/Spring/workspace/bandeirantes/band-ms-administracao/";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/eclipse/workspaceSTS/bandeirantes/band-ms-administracao/";
		public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/SpringSTS/workspacenovo/pasms/pas-ms-mantran/";
		
}
