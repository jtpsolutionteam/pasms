package br.com.jtpsolution.modulos.obj;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.NumberFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabLoteObj {
	
	
	  private Integer id;
	  
	  @NumberFormat(pattern = "#,##0.0000")
      private BigDecimal M3PackingList;
      
	  @NumberFormat(pattern = "#,##0.0000")
      private BigDecimal PesoBrutoDesovado;
      
	  @NumberFormat(pattern = "#,##0.0000")
      private BigDecimal PesoBrutoManifestado;
      
	  @NumberFormat(pattern = "#,##0.0000")
      private BigDecimal DiferencaPeso;
      
	  @NumberFormat(pattern = "#,##0.0000")
      private BigDecimal DiferencaPesoPercentual;
      
      private String Volume;
      
      private Integer QtdeVolume;
      
      private Date DtInicioDesova;
      
      private Date DtFimDesova;
      
      @NumberFormat(pattern = "#,##0.0000")
      private BigDecimal ValorMercadoriaInvoice;
      
      private String NoCE;
      
      private String MercadoriaDesova;
      
      private String NoEscalaNavio;
      
      private String Navio;
      
      private String Viagem;
      
      private Date DtPrevisaoAtracacao;
      
      private Date DtAtracacao;
      
      private String PortoOrigem;
      
      private String LocalAtracacao;
      
      private String Regime;
      
      private String CargaNoTerminalOuMar;
       
      private String NoHBL;
      
      private String NoLoteIDBL;
       
      private Date DtAverbacao;
      
      List<TabLoteContainerObj> Containers;
      
      List<TabLoteAvariasObj> Avarias;
	

}
