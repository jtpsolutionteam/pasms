package br.com.jtpsolution.modulos.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.modulos.obj.TabLoteObj;

@Component
public class InterfaceMarimexService {

	
	@Scheduled(cron = "0 0/2 * * * ?")
	private void InterfaceMarimex() {
		
		String txJson = restconnect.enviaRestGet("/IntegracaoEmpresasService/api/AMALOG/obterProcessos", restconnect.enviaRestLoginMS());
		
		List<TabLoteObj> listRetorno = null;
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			
			if (!txJson.contains("Illegal character")) {
			
			txJson = txJson.replace("$id", "id");
			
			listRetorno = Arrays.asList(mapper.readValue(txJson, TabLoteObj[].class));
			
			if (listRetorno != null) {
				
				for (TabLoteObj atual : listRetorno) {
					
					System.out.println(atual.getNoCE());
					
				}				
			}
			
		 }else {
			 System.out.println("Erro Login Marimex");
		 }
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		System.out.println(txJson);
		
	}
	
}
