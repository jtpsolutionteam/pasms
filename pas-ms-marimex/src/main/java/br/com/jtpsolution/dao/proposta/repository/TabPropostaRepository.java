package br.com.jtpsolution.dao.proposta.repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaObj;



public interface TabPropostaRepository extends JpaRepository<TabPropostaObj, Integer> {

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txTipoProposta = 'DTA' and date(t.dtDtaConclusaoTransito) between date('2020-12-01') and date(now()) and t.ckDtaConclusaoTransito = 0")
	List<TabPropostaObj> findByConclusaoTransitoQuery(Timestamp dtDtaConclusaoTransito);
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and date(t.dtProposta) > '2020-10-01' and t.txLote is not null and t.dtAceite is not null and t.dtDtaConclusaoTransito is null order by t.cdProposta")
	List<TabPropostaObj> findByAceiteLoteConclusaoTransitoNullQuery();

	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	TabPropostaObj findByCdPropostaQuery(Integer cdProposta);
	
	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	List<TabPropostaObj> findByListCdPropostaQuery(Integer cdProposta);
	
	
}