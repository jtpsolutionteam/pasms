package br.com.jtpsolution.dao.conexaorest;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.modulos.obj.TabLoginObj;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;

public class restconnect {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			
			System.out.println(new restconnect().enviaRestLoginMS());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String enviaRestGet(String txUrlcontroller, String txToken) {

		try {

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + txToken);

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			String url = Constants.URL_MARIMEX_PRODUCAO + txUrlcontroller;
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

	public static String enviaRestGet(String txUrlcontroller, String txToken, String txBodyJson) {

		try {

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + txToken);

			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}

			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);

			String url = Constants.URL_MARIMEX_PRODUCAO + txUrlcontroller;
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

	public static String enviaRestPost(String txUrlcontroller, String txToken, String txBodyJson) {

		try {
			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + txToken);

			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}

			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);

			String url = Constants.URL_MARIMEX_PRODUCAO + txUrlcontroller;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

	public static String enviaRestPost(String txUrlcontroller, String txToken, String txBodyJson,
			HttpServletRequest request) {

		try {
			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + txToken);

			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}

			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);

			String url = Constants.URL_MARIMEX_PRODUCAO + txUrlcontroller;

			if (!Validator.isBlankOrNull(request.getParameter("submitFields"))) {

				url += "?submitFields=" + request.getParameter("submitFields");

			}

			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}
	}

	public static String enviaRestPostTimeout(String txUrlcontroller, String txToken, String txBodyJson) {

		try {

			SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();

			factory.setReadTimeout(600 * 1000);

			RestTemplate restTemplate = new RestTemplate();

			restTemplate.setRequestFactory(factory);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + txToken);

			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}

			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);

			String url = Constants.URL_MARIMEX_PRODUCAO + txUrlcontroller;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

	public static String enviaRestPost(String txUrlcontroller, String txToken) {

		try {
			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + txToken);

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			String url = Constants.URL_MARIMEX_PRODUCAO + txUrlcontroller;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

	
	
	public static String enviaRestLoginMS() {

		try {

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			TabLoginObj t = new TabLoginObj();
			t.setLogin("planejamento@amalog.com.br");
			t.setSenha("aMALOG@22");
			
			String txBodyJson = new Gson().toJson(t);
			
			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}

			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);			
			
			String url = Constants.URL_MARIMEX_PRODUCAO + "/MarimexAuthentication/api/usuario/autenticar";
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			String txRetorno = response.getBody();

			JSONObject json = new JSONObject(txRetorno);

			if (!json.isNull("Token")) {

				txRetorno = json.getString("Token");
			}

			return txRetorno;
		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

}
