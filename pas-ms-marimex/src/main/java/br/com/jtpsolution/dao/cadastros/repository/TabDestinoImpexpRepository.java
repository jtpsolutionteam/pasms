package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabDestinoImpexpObj;



public interface TabDestinoImpexpRepository extends JpaRepository<TabDestinoImpexpObj, Integer> {



}