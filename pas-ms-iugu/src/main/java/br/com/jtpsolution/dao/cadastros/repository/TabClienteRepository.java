package br.com.jtpsolution.dao.cadastros.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.TabClienteObj;


public interface TabClienteRepository extends JpaRepository<TabClienteObj, Integer> {

	TabClienteObj findByTxCnpj(String txCnpj);
	
	List<TabClienteObj> findByCdCliente(Integer cdCliente);
	
	@Query("select t from TabClienteObj t where t.cdCliente = ?1")
	TabClienteObj findByIdQuery(Integer cdCliente);

}