package br.com.jtpsolution.dao.cadastros;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_cliente", schema = Constants.SCHEMA)
public class TabClienteObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_cliente")
	// @NotNull(message = "Cliente campo obrigatório!")
	private Integer cdCliente;

	@Column(name = "tx_cliente")
	@NotEmpty(message = "Cliente campo obrigatório!")
	@Size(max = 100, message = "Cliente tamanho máximo de 100 caracteres")
	private String txCliente;
	
	@CNPJ(message = "CNPJ Inválido!")
	@Column(name = "tx_cnpj")
	@NotEmpty(message = "CNPJ campo obrigatório!")
	@Size(max = 20, message = "CNPJ tamanho máximo de 20 caracteres")
	private String txCnpj;
	
	
	@Column(name = "cd_tipo_envio_boleto")
	@NotNull(message = "Tipo de envio do Boleto campo obrigatório!")
	private Integer cdTipoEnvioBoleto;
	
	@Column(name = "cd_tipo_faturamento")
	@NotNull(message = "Tipo de Faturamento campo obrigatório!")
	private Integer cdTipoFaturamento;

	@Column(name = "ck_desconta_comissao")
	@NotNull(message = "Desconta comissão campo obrigatório!")
	private Integer ckDescontaComissao;

	
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txCliente))
			txCliente = txCliente.toUpperCase();
	}



	
	
}
