package br.com.jtpsolution.dao.cadastros;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_impostos", schema = Constants.SCHEMA)
public class TabImpostosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_imposto")
//@NotNull(message = "Imposto campo obrigatório!")
	private Integer cdImposto;

	@Column(name = "tx_imposto")
//@NotEmpty(message = "Imposto campo obrigatório!")
	@Size(max = 30, message = "Imposto tamanho máximo de 30 caracteres")
	private String txImposto;

	@Column(name = "vl_imposto")
//@NotEmpty(message = "Imposto campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlImposto;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txImposto))
			txImposto = txImposto.toUpperCase();
	}

}
