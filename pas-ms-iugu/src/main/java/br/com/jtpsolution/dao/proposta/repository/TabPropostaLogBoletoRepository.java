package br.com.jtpsolution.dao.proposta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.proposta.TabPropostaLogBoletoObj;



public interface TabPropostaLogBoletoRepository extends JpaRepository<TabPropostaLogBoletoObj, Integer> {



}