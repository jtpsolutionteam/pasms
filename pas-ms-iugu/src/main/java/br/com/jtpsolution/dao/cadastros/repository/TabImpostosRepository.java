package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.TabImpostosObj;



public interface TabImpostosRepository extends JpaRepository<TabImpostosObj, Integer> {

	@Query("select t from TabImpostosObj t where t.cdImposto = ?1")
	TabImpostosObj findByCdImpostoQuery(Integer cdImposto);

}