package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaObj;



public interface TabPropostaRepository extends JpaRepository<TabPropostaObj, Integer> {

	TabPropostaObj findByTxBl(String txBl);

	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	List<TabPropostaObj> findByCdPropostaQuery(Integer cdProposta);
	

	@Query("select distinct t from TabPropostaObj t "
			+ "join fetch t.tabClienteObj tabClienteObj  " 	
			+ "where t.txTipoProposta = 'DTA' and t.dtBoletoVencto is not null and t.dtBoletoPagto is null and t.ckBoletoCancelado is null")
	List<TabPropostaObj> findByListDtBoletoPagtoEmAbertoQuery();

	
	@Query("select distinct t from TabPropostaObj t "
			+ "join fetch t.tabClienteObj tabClienteObj  " 	
			+ "where t.txNumeroDta is not null and t.txNumeroDta <> '' and t.ckRotaAmalog = 1 and (t.txBoletoErros is null or t.txBoletoErros = '') and t.dtBoletoVencto is null")
	List<TabPropostaObj> findByListPropostaGeraBoletoQuery();
	

	@Query("select distinct t from TabPropostaObj t "
			+ "join fetch t.tabClienteObj tabClienteObj  " 				
			+ "where t.cdProposta = ?1")
	TabPropostaObj findByCdPropostaNewQuery(Integer cdProposta);
	
}