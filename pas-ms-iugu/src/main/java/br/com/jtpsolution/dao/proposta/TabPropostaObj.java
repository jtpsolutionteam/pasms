package br.com.jtpsolution.dao.proposta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabTipoCarregamentoObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@EntityListeners({GeraBoletoListener.class})
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabPropostaObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "tx_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private String txProposta;
	
	@ManyToOne
	@JoinColumn(name = "cd_cliente")
	@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;

	@Column(name = "dt_proposta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtProposta;

	@ManyToOne
	@JoinColumn(name = "cd_tipo_carregamento")
	@NotNull(message = "Tipo Carregamento campo obrigatório!")
	private TabTipoCarregamentoObj tabTipoCarregamentoObj;

	@Column(name = "tx_tipo_proposta") 
	//@NotEmpty(message = "TipoProposta campo obrigatório!")
	@Size(max = 20, message = "TipoProposta tamanho máximo de 20 caracteres") 
	private String txTipoProposta; 
	
	@Column(name = "vl_frete")
	// @NotEmpty(message = "Frete campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlFrete;

	@Column(name = "vl_emissao_dta")
	//@NotNull(message = "Emissão DTA/DAT campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlEmissaoDta;
	
	@Column(name = "tx_importador") 
	//@NotEmpty(message = "Importador campo obrigatório!")
	@Size(max = 200, message = "Importador tamanho máximo de 200 caracteres") 
	private String txImportador; 
	 
	//@CNPJ(message = "CNPJ inválido!")
	@Column(name = "tx_cnpj") 
	//@NotEmpty(message = "Cnpj campo obrigatório!")
	@Size(max = 20, message = "Cnpj tamanho máximo de 20 caracteres") 
	private String txCnpj; 
	 
	@Column(name = "tx_endereco") 
	//@NotEmpty(message = "Endereco campo obrigatório!")
	@Size(max = 200, message = "Endereco tamanho máximo de 200 caracteres") 
	private String txEndereco; 
	
	@Column(name = "tx_numero") 
	//@NotEmpty(message = "Número campo obrigatório!")
	@Size(max = 6, message = "Número tamanho máximo de 6 caracteres") 
	private String txNumero; 
	 
	@Column(name = "tx_cidade") 
	//@NotEmpty(message = "Cidade campo obrigatório!")
	@Size(max = 50, message = "Cidade tamanho máximo de 50 caracteres") 
	private String txCidade; 
	 
	@Column(name = "tx_uf") 
	//@NotEmpty(message = "Uf campo obrigatório!")
	@Size(max = 2, message = "Uf tamanho máximo de 2 caracteres") 
	private String txUf; 
	 
	@Column(name = "tx_bairro") 
	//@NotEmpty(message = "Bairro campo obrigatório!")
	@Size(max = 50, message = "Bairro tamanho máximo de 50 caracteres") 
	private String txBairro; 
	 
	@Column(name = "tx_cep") 
	//@NotEmpty(message = "Cep campo obrigatório!")
	@Size(max = 10, message = "Cep tamanho máximo de 10 caracteres") 
	private String txCep; 
	
	@Column(name = "vl_frete_previsto")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlFretePrevisto;
	
	@Column(name = "tx_nome_pagador")	
	private String txNomePagador;

	//@CNPJ(message = "CNPJ inválido!")
	@Column(name = "tx_cnpj_importador")	
	private String txCnpjImportador;
	
	@Column(name = "tx_numero_dta")
	private String txNumeroDta;
		
	@Column(name = "tx_bl")
	private String txBl;
		
	@Column(name = "dt_dta_registro") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaRegistro; 
	 
	@Column(name = "dt_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaCarregamento; 
	 
	@Column(name = "dt_dta_parametrizacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaParametrizacao; 	

	@Column(name = "tx_boleto_id") 
	private String txBoletoId;
	
	@Column(name = "dt_boleto_vencto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtBoletoVencto;
	
	@Column(name = "dt_boleto_pagto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtBoletoPagto;

	@Column(name = "tx_boleto_linha_digitavel") 
	private String txBoletoLinhaDigitavel;

	@Column(name = "tx_boleto_url") 
	private String txBoletoUrl;
	
	@Column(name = "tx_boleto_erros") 
	private String txBoletoErros;
	
	@Column(name = "tx_iugu_retornos") 
	private String txIuguRetornos;	
	
	//@Email(message="Email inválido!")
	@Column(name = "tx_email_pagador") 
	private String txEmailPagador;

	@Column(name = "ck_rota_amalog") 
	private Integer ckRotaAmalog;

	@Column(name = "tx_reserva") 
	private String txReserva;
	
	@Column(name = "tx_due") 
	private String txDue;	
	
	@Column(name = "ck_boleto_cancelado") 
	private Integer ckBoletoCancelado;
	
	@Column(name = "dt_ultima_tentativa_boleto") 	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtUltimaTentativaBoleto;
	
	

}
