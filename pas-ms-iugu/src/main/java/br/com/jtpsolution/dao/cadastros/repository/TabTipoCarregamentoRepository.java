package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabTipoCarregamentoObj;



public interface TabTipoCarregamentoRepository extends JpaRepository<TabTipoCarregamentoObj, Integer> {



}