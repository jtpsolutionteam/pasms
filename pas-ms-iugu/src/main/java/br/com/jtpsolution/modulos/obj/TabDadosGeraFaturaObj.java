package br.com.jtpsolution.modulos.obj;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabDadosGeraFaturaObj {

	private String txEmailPagador;
	
	private Date dtVencto;
	
	private String txPropostas;
	
	private Double vlTotalLote;

	private String txCnpj;
	
	private String txNomePagador;

	private Integer cdFaturamentoLote;

	private String txEndereco;
	
	private String txNumero;
	
	private String txBairro;
	
	private String txCidade;
	
	private String txUf;
	
	private String txCep;

	
	
}
