package br.com.jtpsolution.modulos.service;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.listener.GeraBoletoListener;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;

@Component
public class VerificaPagamentosIuguBean {

	@Autowired
	private TabPropostaRepository tabPropostaRepository;
	
	@Autowired
	private GeraBoletoListener geraBoletoListener;

	@Scheduled(cron = "0 0/30 6-23 * * ?")
	public void criarBoletos() {

		try {
			List<TabPropostaObj> lst = tabPropostaRepository.findByListPropostaGeraBoletoQuery();

			System.out.println("IUGU Gera Boleto: Inicio - " + GeneralParser.format_dateHSBR(new Date()));
			for (TabPropostaObj atual : lst) {

				TabPropostaObj tabProposta = tabPropostaRepository.findByCdPropostaNewQuery(atual.getCdProposta());

				if (tabProposta != null) {
					
					geraBoletoListener.geraboleto(tabProposta);
					
					tabProposta.setDtUltimaTentativaBoleto(new Date());
					//tabProposta.setTxIuguRetornos(".");
					tabPropostaRepository.save(tabProposta);
				}
				Thread.sleep(1000);
			}
			System.out.println("IUGU Gera Boleto: FIM - " + GeneralParser.format_dateHSBR(new Date()));
		} catch (Exception ex) {
			System.out.println("CriarBoletos erro :" + ex.getMessage());
		}
	}

	@Scheduled(cron = "0 0/60 6-23 * * ?")
	public void baixapagamentos() {

		try {

			List<TabPropostaObj> lst = tabPropostaRepository.findByListDtBoletoPagtoEmAbertoQuery();
			// List<TabPropostaObj> lst = tabPropostaRepository.findByCdPropostaQuery(8400);

			for (TabPropostaObj atual : lst) {

				if (!Validator.isBlankOrNull(atual.getTxBoletoId())) {

					System.out.println("baixapagamentos: " + atual.getCdProposta());

					String retorno = restconnect.enviaRestGet("https://api.iugu.com/v1/invoices/"
							+ atual.getTxBoletoId() + "?api_token=" + Constants.IUGU_KEY_PRODUCAO);

					JSONObject json = new JSONObject(retorno);

					if (!json.isNull("status")) {

						String txStatus = json.getString("status");
						// System.out.println(txStatus);

						if (txStatus.equals("paid")) {

							String dtPagto = json.getString("paid_at").substring(0, 10);
							// System.out.println(dtPagto);

							atualizaPagamentoProposta(atual.getCdProposta(),
									GeneralParser.parseDate("yyyy-MM-dd", dtPagto), null);

						} else if (txStatus.equals("canceled")) {

							atualizaPagamentoProposta(atual.getCdProposta(), null, 1);

						}

					}

					Thread.sleep(2000);
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Baixa de Pagamento - IUGU - " + e.getMessage());
		}
	}

	private void atualizaPagamentoProposta(Integer cdProposta, Date dtPagto, Integer ckCancelado) {

		TabPropostaObj tab = tabPropostaRepository.findByCdPropostaNewQuery(cdProposta);

		try {

			if (tab != null) {
				if (!Validator.isBlankOrNull(dtPagto)) {
					tab.setDtBoletoPagto(dtPagto);
				} else if (!Validator.isBlankOrNull(ckCancelado)) {
					tab.setCkBoletoCancelado(ckCancelado);
				}
				tabPropostaRepository.save(tab);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
