package br.com.jtpsolution.modulos.listener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabImpostosObj;
import br.com.jtpsolution.dao.cadastros.repository.TabImpostosRepository;
import br.com.jtpsolution.dao.proposta.TabPropostaLogBoletoObj;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaLogBoletoRepository;
import br.com.jtpsolution.modulos.obj.TabIuguCustomVariablesObj;
import br.com.jtpsolution.modulos.obj.TabIuguFaturaClienteEnderecoObj;
import br.com.jtpsolution.modulos.obj.TabIuguFaturaClienteObj;
import br.com.jtpsolution.modulos.obj.TabIuguFaturaItemObj;
import br.com.jtpsolution.modulos.obj.TabIuguFaturaObj;
import br.com.jtpsolution.modulos.service.PagamentosIuguService;
import br.com.jtpsolution.modulos.service.TabClienteService;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Service
public class GeraBoletoListener {

	// @Autowired
	PagamentosIuguService pagamentosIuguService = new PagamentosIuguService();

	@Autowired
	private TabPropostaLogBoletoRepository tabPropostaLogBoletoRepository;

	@Autowired
	private TabClienteService tabClienteService;

	@Autowired
	private TabImpostosRepository tabImpostosRepository;

	// @PreUpdate
	public void geraboleto(TabPropostaObj tabPropostaObj) {

		// AutowireHelper.autowire(this, this.tabPropostaLogBoletoRepository);
		// AutowireHelper.autowire(this, this.tabClienteService);
		// AutowireHelper.autowire(this, this.tabImpostosRepository);

		TabClienteObj tabClienteObj = tabClienteService.consultar(tabPropostaObj.getTabClienteObj().getCdCliente());

		String txCnpjCliente = GeneralUtil.TiraNaonumero(tabClienteObj.getTxCnpj());
		String txCnpjPagador = GeneralUtil.TiraNaonumero(tabPropostaObj.getTxCnpj());

		if (!txCnpjCliente.equals(txCnpjPagador)) { // IMPORTADOR

			if (Validator.isValidCNPJ(txCnpjCliente) && Validator.isValidCNPJ(txCnpjPagador)) {

				if (tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 1
						|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 3) {
					if (!Validator.isBlankOrNull(tabPropostaObj.getTxNumeroDta())
							&& tabPropostaObj.getCkRotaAmalog() == 1
							&& Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {
						System.out.println("IUGU Gera Boleto: Importador - " + tabPropostaObj.getCdProposta());
						envioBoleto(tabPropostaObj, tabClienteObj);
					}
				} else {
					if (!Validator.isBlankOrNull(tabPropostaObj.getTxReserva()) && tabPropostaObj.getCkRotaAmalog() == 1
							&& Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {
						System.out.println("IUGU Gera Boleto: Importador - " + tabPropostaObj.getCdProposta());
						envioBoleto(tabPropostaObj, tabClienteObj);
					}
				}
			}else {
				
				if (Validator.isValidCPF(txCnpjPagador)) {

					if (tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 1
							|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 3) {
						if (!Validator.isBlankOrNull(tabPropostaObj.getTxNumeroDta())
								&& tabPropostaObj.getCkRotaAmalog() == 1
								&& Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {
							System.out.println("IUGU Gera Boleto: Importador - " + tabPropostaObj.getCdProposta());
							envioBoleto(tabPropostaObj, tabClienteObj);
						}
					}else {
						if (!Validator.isBlankOrNull(tabPropostaObj.getTxReserva())
								&& tabPropostaObj.getCkRotaAmalog() == 1
								&& Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {
							System.out.println("IUGU Gera Boleto: Importador - " + tabPropostaObj.getCdProposta());
							envioBoleto(tabPropostaObj, tabClienteObj);
						}					
					}
				}	
			}

		} else { // NVOCC

			if (tabClienteObj.getCdTipoFaturamento() == 1) {

				if (!Validator.isBlankOrNull(tabPropostaObj.getDtDtaParametrizacao())
						&& tabPropostaObj.getCkRotaAmalog() == 1
						&& Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {

					System.out.println("IUGU Gera Boleto: NVOCC - " + tabPropostaObj.getCdProposta());
					envioBoleto(tabPropostaObj, tabClienteObj);

				}

			} else if (tabClienteObj.getCdTipoEnvioBoleto() == 2) {

				if (!Validator.isBlankOrNull(tabPropostaObj.getDtDtaCarregamento())
						&& tabPropostaObj.getCkRotaAmalog() == 1
						&& Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {

					System.out.println("IUGU Gera Boleto: NVOCC - " + tabPropostaObj.getCdProposta());
					envioBoleto(tabPropostaObj, tabClienteObj);

				}
			}
		}
	}

	private double verificaValor(TabClienteObj tabClienteObj, double vlEmissaoDta, double vlFrete) {

		TabImpostosObj tabImpostosObj = tabImpostosRepository.findByCdImpostoQuery(1);

		if (!Validator.isBlankOrNull(tabClienteObj.getCkDescontaComissao())) {
			if (tabClienteObj.getCkDescontaComissao() == 1) {

				if (!Validator.isBlankOrNull(vlEmissaoDta)) {
					if (vlEmissaoDta > 0) {
						vlEmissaoDta = (vlEmissaoDta
								- ((vlEmissaoDta * tabImpostosObj.getVlImposto().doubleValue()) / 100));
						vlFrete = (vlFrete - vlEmissaoDta);

					}
				}
			}
		}

		return vlFrete;
	}

	private void envioBoleto(TabPropostaObj tabPropostaObj, TabClienteObj tabClienteObj) {

		Date dtVencto = GeneralParser.retornaDataPosteriorDiasCorridosUteis(new Date(), 7);

		TabIuguFaturaObj t = new TabIuguFaturaObj();
		//Capa Fatura
		t.setEnsure_workday_due_date(false);
		t.setEmail(GeneralUtil.arrumaEmailCC(tabPropostaObj.getTxEmailPagador(),0));
		if (!Validator.isBlankOrNull(GeneralUtil.arrumaEmailCC(tabPropostaObj.getTxEmailPagador(),1))) {
			t.setCc_emails(GeneralUtil.arrumaEmailCC(tabPropostaObj.getTxEmailPagador(),1));
		}
		
		t.setDue_date(GeneralParser.format_dateBRMySql(dtVencto));
		t.setNotification_url("https://amalog.com.br/retornosiugu/" + tabPropostaObj.getCdProposta());
		
		//Itens Fatura
		List<TabIuguFaturaItemObj> lstItem = new ArrayList<TabIuguFaturaItemObj>();
		TabIuguFaturaItemObj it = new TabIuguFaturaItemObj();
		
		if (tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 1
				|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 3) {
			
			it.setDescription("Amalog Proposta " + tabPropostaObj.getTxProposta() + " - BL: "
					+ tabPropostaObj.getTxBl() + " - DTA: " + tabPropostaObj.getTxNumeroDta());
			it.setQuantity(1);
			
		} else {
			it.setDescription("Amalog Proposta " + tabPropostaObj.getTxProposta() + " - Reserva: "
					+ tabPropostaObj.getTxReserva());
			it.setQuantity(1);
		}
		
		if (tabPropostaObj.getVlFrete() != null) {

			double vlFrete = verificaValor(tabClienteObj,
					tabPropostaObj.getVlEmissaoDta() != null ? tabPropostaObj.getVlEmissaoDta() : 0,
					tabPropostaObj.getVlFrete());

			it.setPrice_cents(GeneralParser.parseInt(GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney(vlFrete))));
			
		} else {

			double vlFrete = verificaValor(tabClienteObj,
					tabPropostaObj.getVlEmissaoDta() != null ? tabPropostaObj.getVlEmissaoDta() : 0,
					tabPropostaObj.getVlFretePrevisto());
			it.setPrice_cents(GeneralParser.parseInt(GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney(vlFrete))));
		}		
		lstItem.add(it);		
		t.setItems(lstItem);
		
		//Tipo de Pagamento
		String[] arrayPagto = {"bank_slip", "pix"};		
		t.setPayable_with(arrayPagto);
		
		//Variaveis Customizadas
		List<TabIuguCustomVariablesObj> lstTv = new ArrayList<TabIuguCustomVariablesObj>();
		TabIuguCustomVariablesObj tv = new TabIuguCustomVariablesObj();
		tv.setName("cdProposta");
		tv.setValue(String.valueOf(tabPropostaObj.getCdProposta()));
		t.setCustom_variables(lstTv);
		
		//Cliente Pagamento
		TabIuguFaturaClienteObj tc = new TabIuguFaturaClienteObj();
		tc.setCpf_cnpj(GeneralUtil.TiraNaonumero(tabPropostaObj.getTxCnpj()));
		tc.setName(GeneralParser.convertISO8859ToUTF8(tabPropostaObj.getTxNomePagador()));
		tc.setPhone("32191571");
		tc.setPhone_prefix("13");
		
		 TabIuguFaturaClienteEnderecoObj tce = new TabIuguFaturaClienteEnderecoObj();
		 tce.setStreet(tabPropostaObj.getTxEndereco());
		 tce.setNumber(tabPropostaObj.getTxNumero());
		 //tce.setComplement("ap 154");
		 tce.setDistrict(tabPropostaObj.getTxBairro());
		 tce.setCity(tabPropostaObj.getTxCidade());
		 tce.setState(tabPropostaObj.getTxUf());
		 tce.setCountry("Brasil");
		 tce.setZip_code(tabPropostaObj.getTxCep());
		 tc.setAddress(tce);
		 
		 t.setPayer(tc);
		
		Gson g = new Gson();
		
		/*
		HashMap h = new HashMap();
		h.put("api_token", Constants.IUGU_KEY_PRODUCAO);
		h.put("email", GeneralUtil.arrumaEmailCC(tabPropostaObj.getTxEmailPagador(), 0)); // receb.getTx_email());
		if (!Validator.isBlankOrNull(GeneralUtil.arrumaEmailCC(tabPropostaObj.getTxEmailPagador(), 1))) {
			h.put("cc_emails", GeneralUtil.arrumaEmailCC(tabPropostaObj.getTxEmailPagador(), 1));
		}
		h.put("due_date", GeneralParser.format_dateBRMySql(dtVencto));
		h.put("payable_with", txTipoFatura);
		if (tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 1
				|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 3) {
			h.put("items[][description]", "Amalog Proposta " + tabPropostaObj.getTxProposta() + " - BL: "
					+ tabPropostaObj.getTxBl() + " - DTA: " + tabPropostaObj.getTxNumeroDta());
			h.put("items[][quantity]", "1");
		} else {
			h.put("items[][description]", "Amalog Proposta " + tabPropostaObj.getTxProposta() + " - Reserva: "
					+ tabPropostaObj.getTxReserva());
			h.put("items[][quantity]", "1");
		}

		if (tabPropostaObj.getVlFrete() != null) {

			double vlFrete = verificaValor(tabClienteObj,
					tabPropostaObj.getVlEmissaoDta() != null ? tabPropostaObj.getVlEmissaoDta() : 0,
					tabPropostaObj.getVlFrete());

			h.put("items[][price_cents]", GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney((vlFrete))));
		} else {

			double vlFrete = verificaValor(tabClienteObj,
					tabPropostaObj.getVlEmissaoDta() != null ? tabPropostaObj.getVlEmissaoDta() : 0,
					tabPropostaObj.getVlFretePrevisto());

			h.put("items[][price_cents]", GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney((vlFrete))));
		}

		h.put("payer[cpf_cnpj]", GeneralUtil.TiraNaonumero(tabPropostaObj.getTxCnpj()));
		h.put("payer[name]", GeneralParser.convertISO8859ToUTF8(tabPropostaObj.getTxNomePagador()));
		h.put("payer[phone_prefix]", "13");
		h.put("payer[phone]", "32191571");

		h.put("payer[custom_variables][name]", "cdProposta");
		h.put("payer[custom_variables][value]", tabPropostaObj.getCdProposta());
		h.put("notification_url", "https://amalog.com.br/retornosiugu/" + tabPropostaObj.getCdProposta());

		h.put("notification_url", "https://amalog.com.br/retornosiugu/" + tabPropostaObj.getCdProposta());
		h.put("payer[address][street]", tabPropostaObj.getTxEndereco());
		h.put("payer[address][number]", tabPropostaObj.getTxNumero());
		h.put("payer[address][district]", tabPropostaObj.getTxBairro());
		h.put("payer[address][city]", tabPropostaObj.getTxCidade());
		h.put("payer[address][state]", tabPropostaObj.getTxUf());
		h.put("payer[address][country]", "Brasil");
		h.put("payer[address][zip_code]", tabPropostaObj.getTxCep());
		*/

		pagamentosIuguService.CriarFatura(g.toJson(t));
		String txFaturaId = pagamentosIuguService.getTxIdFatura();

		if (pagamentosIuguService.getTxMensagem().contains("sucesso")) {

			tabPropostaObj.setDtBoletoVencto(dtVencto);
			tabPropostaObj.setTxBoletoId(txFaturaId);
			tabPropostaObj.setTxBoletoLinhaDigitavel(pagamentosIuguService.getTxBoletoLinhaDigitavel());
			tabPropostaObj.setTxBoletoUrl(pagamentosIuguService.getTxBoletoUrl());
			tabPropostaObj.setTxBoletoErros(null);
			// System.out.println(txFaturaId);

			TabPropostaLogBoletoObj Tab = new TabPropostaLogBoletoObj();
			Tab.setCdProposta(tabPropostaObj.getCdProposta());
			Tab.setDtVencto(dtVencto);
			Tab.setDtEnvio(new Date());
			tabPropostaLogBoletoRepository.save(Tab);

		} else {

			tabPropostaObj.setTxBoletoErros(pagamentosIuguService.getTxMensagem().replace("\\u00e3", "ã")
					.replace("\\u00e9", "é").replace("\\u00e1", "á"));
		}

	}

}
