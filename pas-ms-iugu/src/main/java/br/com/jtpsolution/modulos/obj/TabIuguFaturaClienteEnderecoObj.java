package br.com.jtpsolution.modulos.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabIuguFaturaClienteEnderecoObj {

	private String zip_code;
	
	private String street;
	
	private String number;
	
	private String district;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String complement;
	
	
	
}
