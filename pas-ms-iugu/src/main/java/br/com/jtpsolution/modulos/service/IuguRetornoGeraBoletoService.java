package br.com.jtpsolution.modulos.service;

import java.util.Date;
import java.util.HashMap;

import org.springframework.stereotype.Service;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.modulos.obj.TabDadosGeraFaturaObj;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class IuguRetornoGeraBoletoService {

	private String txFaturaId;
	
	private Date dtVencto;
	
	private String txBoletoLinhaDigitavel;
	
	private String txBoletoUrl;
	
	private String txBoletoErros;
	
	public void envioBoleto(TabDadosGeraFaturaObj tabDadosGeraFaturaObj, String txPropostas, Integer cdFaturamentoLote, Double vlTotalLote, String txTipoFatura) {
		
		PagamentosIuguService pagamentosIuguService = new PagamentosIuguService();

		Date dtVencto = GeneralParser.retornaDataPosteriorDiasCorridosUteis(new Date(), 7);

		HashMap h = new HashMap();
		h.put("api_token", Constants.IUGU_KEY_PRODUCAO);
		h.put("email", tabDadosGeraFaturaObj.getTxEmailPagador()); // receb.getTx_email());
		if (!Validator.isBlankOrNull(GeneralUtil.arrumaEmailCC(tabDadosGeraFaturaObj.getTxEmailPagador(),1))) {
			  h.put("cc_emails", GeneralUtil.arrumaEmailCC(tabDadosGeraFaturaObj.getTxEmailPagador(),1));
		}

		h.put("due_date", GeneralParser.format_dateBRMySql(dtVencto));
		
		h.put("payable_with", txTipoFatura);
			h.put("items[][description]", txPropostas);
			h.put("items[][quantity]", "1");
			h.put("items[][price_cents]",
					GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney(vlTotalLote)));

		h.put("payer[cpf_cnpj]", GeneralUtil.TiraNaonumero(tabDadosGeraFaturaObj.getTxCnpj()));
		h.put("payer[name]", GeneralParser.convertISO8859ToUTF8(tabDadosGeraFaturaObj.getTxNomePagador()));
		h.put("payer[phone_prefix]", "13");
		h.put("payer[phone]", "32191571");

		h.put("payer[custom_variables][name]", "cdFaturamentoLote");
		h.put("payer[custom_variables][value]", cdFaturamentoLote);
		h.put("notification_url", "https://amalog.com.br/retornosiugu/lote/" + cdFaturamentoLote);

		h.put("notification_url", "https://amalog.com.br/retornosiugu/lote/" + cdFaturamentoLote);
		h.put("payer[address][street]", tabDadosGeraFaturaObj.getTxEndereco());
		h.put("payer[address][number]", tabDadosGeraFaturaObj.getTxNumero());
		h.put("payer[address][district]", tabDadosGeraFaturaObj.getTxBairro());
		h.put("payer[address][city]", tabDadosGeraFaturaObj.getTxCidade());
		h.put("payer[address][state]", tabDadosGeraFaturaObj.getTxUf());
		h.put("payer[address][country]", "Brasil");
		h.put("payer[address][zip_code]", tabDadosGeraFaturaObj.getTxCep());

		pagamentosIuguService.CriarFatura(h);
		String txFaturaId = pagamentosIuguService.getTxIdFatura();

		if (pagamentosIuguService.getTxMensagem().contains("sucesso")) {

			this.setDtVencto(dtVencto);
			this.setTxFaturaId(txFaturaId);
			this.setTxBoletoLinhaDigitavel(pagamentosIuguService.getTxBoletoLinhaDigitavel());
			this.setTxBoletoUrl(pagamentosIuguService.getTxBoletoUrl());
			this.setTxBoletoErros(null);
			
		} else {

			this.setTxBoletoErros(pagamentosIuguService.getTxMensagem());
			
		}

	}

	
	
}
