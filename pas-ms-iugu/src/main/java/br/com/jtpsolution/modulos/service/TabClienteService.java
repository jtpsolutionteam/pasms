package br.com.jtpsolution.modulos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.repository.TabClienteRepository;
import br.com.jtpsolution.util.GeneralParser;

@Service
public class TabClienteService {


	@Autowired
	private TabClienteRepository  tabRepository;

	
	
	public List<TabClienteObj> listar() {
		return tabRepository.findAll();
		
	}
	
	/* Modelo tela filho 
	
		public List<VwTabAgenteObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	/* Modelo - Tela Pesquisar
			public Page<VwTabAgenteObj> telapesquisar(TabFiltroObj tabFiltroObj, Pageable pageable) {

			Page<VwTabAgenteObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
			return Tab;

		}
	*/


	public TabClienteObj gravar(TabClienteObj Tab) {
		
		boolean ckAlteracao = false;

		TabClienteObj tNovo =  tabRepository.save(Tab);	
		
		return tNovo;
		
	}

	public TabClienteObj consultar(Integer cdCliente) {
	 TabClienteObj Tab = tabRepository.findByIdQuery(cdCliente);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdAgente) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabAgenteObj Tab = tabRepository.findOne(CdAgente);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	

	
}
