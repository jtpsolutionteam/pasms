package br.com.jtpsolution.modulos.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabIuguFaturaItemObj {

	
	private String description;
    
	private Integer quantity;
	
    private Integer price_cents;
	
	
	
}
