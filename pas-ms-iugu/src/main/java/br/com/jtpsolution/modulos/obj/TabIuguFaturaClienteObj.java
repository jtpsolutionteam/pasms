package br.com.jtpsolution.modulos.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabIuguFaturaClienteObj {

	private String name;
	
	private String phone_prefix;
	
    private String cpf_cnpj;
    
    private String phone;
    
    private String email;
	
	private TabIuguFaturaClienteEnderecoObj address;
	
}
