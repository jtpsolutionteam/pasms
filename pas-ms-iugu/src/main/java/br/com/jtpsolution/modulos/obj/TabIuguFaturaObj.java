package br.com.jtpsolution.modulos.obj;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabIuguFaturaObj {

	public static void main(String[] args) {
		
		TabIuguFaturaObj t = new TabIuguFaturaObj();
		//Capa Fatura
		t.setEnsure_workday_due_date(false);
		t.setEmail("sergioaugusto@jtpsolution.com.br");
		t.setCc_emails("contato@jtpsolution.com.br");
		t.setDue_date("2021-07-15");
		
		//Itens Fatura
		List<TabIuguFaturaItemObj> lstItem = new ArrayList<TabIuguFaturaItemObj>();
		TabIuguFaturaItemObj it = new TabIuguFaturaItemObj();
		it.setDescription("teste");
		it.setQuantity(1);
		it.setPrice_cents(100);
		lstItem.add(it);		
		t.setItems(lstItem);
		
		//Tipo de Pagamento
		String[] arrayPagto = {"bank_slip", "pix"};		
		t.setPayable_with(arrayPagto);
		
		//Cliente Pagamento
		TabIuguFaturaClienteObj tc = new TabIuguFaturaClienteObj();
		tc.setCpf_cnpj("04746455767");
		tc.setEmail("sergioaugusto@jtpsolution.com.br");
		tc.setName("sergio");
		tc.setPhone("981732300");
		tc.setPhone_prefix("13");
		
		 TabIuguFaturaClienteEnderecoObj tce = new TabIuguFaturaClienteEnderecoObj();
		 tce.setStreet("avenida");
		 tce.setNumber("82");
		 tce.setComplement("ap 154");
		 tce.setDistrict("gonzaga");
		 tce.setCity("santos");
		 tce.setState("sp");
		 tce.setCountry("brasil");
		 tce.setZip_code("11060400");
		 tc.setAddress(tce);
		 
		 t.setPayer(tc);
		
		Gson g = new Gson();
		
		System.out.println(g.toJson(t));
		
	}
	
	
	private boolean ensure_workday_due_date;
	
	private String email;
	
	private String cc_emails;

	private String due_date;
	
	private String notification_url;
	
	private List<TabIuguFaturaItemObj> items;
	
	private List<TabIuguCustomVariablesObj> custom_variables;
	
	private String[] payable_with;
	
	private TabIuguFaturaClienteObj payer;
	
}
