package br.com.jtpsolution;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

@EnableEurekaServer
@SpringBootApplication
public class PasEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PasEurekaApplication.class, args);
	}
	
	@Bean
	public LocaleResolver localeResolver() {

		return new FixedLocaleResolver(new Locale("pt", "BR"));

	}

}
