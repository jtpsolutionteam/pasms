package br.com.jtpsolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PasAuthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PasAuthServerApplication.class, args);
	}

}
