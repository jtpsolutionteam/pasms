package br.com.jtpsolution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

	@Autowired
	private TabUsuarioRepository tabUsuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub

		TabUsuarioObj tUsuario = tabUsuarioRepository.findByTxEmailAndCkAtivo(email, 1);

		if (tUsuario == null) {
			throw new UsernameNotFoundException("Usuário e/ou senha inválidos!");
		}

		return new AuthUser(tUsuario.getTxEmail(), tUsuario.getTxSenha(), AuthorityUtils.createAuthorityList(),
				tUsuario.getCdUsuario(), tUsuario.getCdGrupoVisao(), tUsuario.getTxApelido(), tUsuario.getTxEmail(),
				tUsuario.getCdGrupoAcesso(), tUsuario.getTxNome());
	}

}
