package br.com.jtpsolution;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;

@Getter
public class AuthUser extends User {

	private Integer cdUsuario;
	private Integer cdGrupoVisao;

	private Integer cdGrupoAcesso;
	private String txApelido;
	private String txEmail;
	private String txNome;
	private String txSenha;

	private static final long serialVersionUID = 1L;

	public AuthUser(String username, String password, Collection<? extends GrantedAuthority> authorities,
			Integer cdUsuario, Integer cdGrupoVisao, String txApelido, String txEmail, Integer cdGrupoAcesso,
			String txNome) {
		super(username, password, authorities);
		this.cdUsuario = cdUsuario;
		this.cdGrupoVisao = cdGrupoVisao;
		this.txApelido = txApelido;
		this.txEmail = txEmail;
		this.cdGrupoAcesso = cdGrupoAcesso;
		this.txNome = txNome;
		this.txSenha = password;
	}

	public String getTxSenha() {
		return txSenha;
	}

	public Integer getCdUsuario() {
		return this.cdUsuario;
	}

	public String getTxApelido() {
		return txApelido;
	}

	public String getTxEmail() {
		return txEmail;
	}

	public Integer getCdGrupoVisao() {
		return cdGrupoVisao;
	}

	public Integer getCdGrupoAcesso() {
		return cdGrupoAcesso;
	}

	public String getTxNome() {
		return txNome;
	}

}