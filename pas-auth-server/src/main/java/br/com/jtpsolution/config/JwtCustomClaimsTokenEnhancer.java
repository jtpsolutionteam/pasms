package br.com.jtpsolution.config;



import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import br.com.jtpsolution.AuthUser;

public class JwtCustomClaimsTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		if (authentication.getPrincipal() instanceof AuthUser) {
			AuthUser authUser = (AuthUser) authentication.getPrincipal();
			
			Map<String, Object> info = new HashMap<String, Object>();
			info.put("cdUsuario", authUser.getCdUsuario());
			info.put("cdGrupoVisao", authUser.getCdGrupoVisao());
			info.put("cdGrupoAcesso", authUser.getCdGrupoAcesso());
			info.put("txApelido", authUser.getTxApelido());
			info.put("txNome", authUser.getTxNome());
			info.put("txEmail", authUser.getTxEmail());
			info.put("txSenha", authUser.getTxSenha());
			
			
			
			DefaultOAuth2AccessToken oAuth2AccessToken = (DefaultOAuth2AccessToken) accessToken;
			oAuth2AccessToken.setAdditionalInformation(info);
		}
		
		return accessToken;
	}

}