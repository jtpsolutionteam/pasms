package br.com.jtpsolution;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TabUsuarioRepository extends JpaRepository<TabUsuarioObj, Integer> {

	TabUsuarioObj findByTxEmail(String email);

	TabUsuarioObj findByTxEmailAndTxSenha(String email, String senha);

	TabUsuarioObj findByTxEmailAndTxSenhaAndCkAtivo(String email, String senha, Integer ativo);

	TabUsuarioObj findByTxEmailAndCkAtivo(String txEmail, Integer ckAtivo);

}
