package br.com.jtpsolution.modulos.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.jtpsolution.dao.robos.VwRobosStatusObj;
import br.com.jtpsolution.dao.robos.repository.VwRobosStatusRepository;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.linux.linuxService;

@Component
public class VerificaRobosService {

	private String msDta = "pas-ms-dta.jar";
	
	private String msMantran = "pas-ms-mantran.jar";
	
	private String nohupDta = "nohup java -jar /sistemas/pas-ms-dta.jar > nohupdta &";

	private String nohupMantran = "nohup java -jar /sistemas/pas-ms-mantran.jar > nohupmantran &";

	@Autowired
	private VwRobosStatusRepository vwRobosStatusRepository;
	
	@Scheduled(cron = "0 0/25 7-23 * * ?")
	public void verificaRobos() {
		
		List<VwRobosStatusObj> lst = vwRobosStatusRepository.findAll();
		
		for (VwRobosStatusObj atual : lst) {
			
			System.out.println("Verifica Robos - Inicio " + GeneralParser.format_dateHSBR(new Date()));
			
			if (!atual.getTxStatusDta().equals("LIGADO")) {
				
				System.out.println("Robo DTA: Desligado" + GeneralParser.format_dateHSBR(new Date()));
				linuxService l = new linuxService();
				try {
					
					l.executarServico("ps aux | grep -i "+msDta+" | awk {'print $2'} | xargs kill -9");
					Thread.sleep(4000);
					l.executarServico(nohupDta); 
				    System.out.println("Robo DTA: Ligado" + GeneralParser.format_dateHSBR(new Date()));
					
				 /*	
				 String txPid = l.consultarServico("ps -ef| grep java", msDta);
				 
				 if (!Validator.isBlankOrNull(txPid)) {
				    l.executarServico("kill -9 "+txPid);
				    Thread.sleep(4000);
				    l.executarServico(nohupDta); 
				    System.out.println("Robo DTA: Ligado" + GeneralParser.format_dateHSBR(new Date()));
				 }else {
					 l.executarServico(nohupDta); 
					 System.out.println("Robo DTA: Ligado" + GeneralParser.format_dateHSBR(new Date()));
				 }	
				 
				 */			 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("erro linux :"+e.getMessage());
				}
				
				
			}

			if (!atual.getTxStatusMantran().equals("LIGADO")) {

				System.out.println("Robo Mantran: Desligado" + GeneralParser.format_dateHSBR(new Date()));
				linuxService l = new linuxService();
				try {

					l.executarServico("ps aux | grep -i "+msMantran+" | awk {'print $2'} | xargs kill -9");
					Thread.sleep(4000);
					l.executarServico(nohupMantran); 
				    System.out.println("Robo Mantran: Ligado" + GeneralParser.format_dateHSBR(new Date()));
					
				 /*	
				 String txPid = l.consultarServico("ps -ef| grep java", msMantran);
				 
				 if (!Validator.isBlankOrNull(txPid)) {
				    l.executarServico("kill -9 "+txPid);
				    Thread.sleep(4000);
				    l.executarServico(nohupMantran); 
				    System.out.println("Robo Mantran: Ligado" + GeneralParser.format_dateHSBR(new Date()));
				 }else {
					 l.executarServico(nohupMantran); 
					 System.out.println("Robo Mantran: Ligado" + GeneralParser.format_dateHSBR(new Date()));
				 }	
				 */			 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("erro linux :"+e.getMessage());
				}
			}
		}
		
		System.out.println("Verifica Robos - Fim " + GeneralParser.format_dateHSBR(new Date()));
	}
	
	
}
