package br.com.jtpsolution.modulos.controller;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/notificacoes")
public class NotificacoesController {

	
  @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
  public void recebeNotificacao(@RequestBody String object) {
	  try {
		  
	  ObjectMapper mapper = new ObjectMapper();	 
	  Map<String,Object> map = mapper.readValue(object, Map.class);
	  
	  Set<String> chave = new HashSet<String>(map.keySet());
		
	  for(String e:chave) {
			System.out.println("Chave: " + e + " Valor: " + map.get(e));
			
			if (e.startsWith("tab")) {				
				
				String[] words = map.get(e).toString().split(",");
				List<String> wordList = Arrays.asList(words);  

				for (String t : wordList) {
					
					System.out.println(t.replace("{", "").replace("}", ""));
					
				}
				
				Set<String> chaveTab = new HashSet<String>(Arrays.asList(words));
				
				/*
				Set<String> chaveTab = new HashSet<String>();
				String[] s = map.get(e).toString().split(",");
				chaveTab.add(s.toString());
				
				
				for(String eTab:chaveTab) {
					System.out.println("Chave: " + eTab + " Valor: " + map.get(eTab));
				}*/
			}
	  }
	
	  
	  verificaObjeto(object, "cdStatus");
	  
	} catch (JsonMappingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JsonProcessingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  
	  
	  
  }
	

  
  
	private String verificaObjeto(Object objNew, String txCampoVerificacao) {

		String txResposta = "";
		try {

			Field[] camposNew = objNew.getClass().getDeclaredFields();

			for (int i = 0; i < camposNew.length; i++) {
				// Tipo
				// Exemplo: class java.math.BigDecimal
				// System.out.println(campos[i].getType());
				camposNew[i].setAccessible(true);
				
				String txNomeCampo = camposNew[i].getName();
				String txValor = "";
				
				if (camposNew[i].get(objNew) != null) {
					txValor = camposNew[i].get(objNew).toString();
				}
				System.out.println(txNomeCampo);
				System.out.println(txValor);
				
				if (txNomeCampo.equals(txCampoVerificacao)) {
					txResposta = txValor;
					break;
				}
				
				if (txValor.contains("br.com")) {
					Field[] campos = camposNew[i].get(objNew).getClass().getDeclaredFields();
					for (int i2 = 0; i2 < campos.length; i2++) {
						campos[i2].setAccessible(true);
						txNomeCampo = campos[i2].getName();
						if (campos[i2].get(camposNew[i].get(objNew)) != null) {
						  txValor = campos[i2].get(camposNew[i].get(objNew)).toString();
						}
						System.out.println(txNomeCampo);
						System.out.println(txValor);
						
						if (txNomeCampo.equals(txCampoVerificacao)) {
							txResposta = txValor;
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("Erro "+ex.getClass().getSimpleName()+" - "+ex.getMessage());
		}

		return txResposta;

	}
  
	
}
