package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaObj;



public interface TabPropostaRepository extends JpaRepository<TabPropostaObj, Integer> {

	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	TabPropostaObj findByCdPropostaQuery(Integer cdProposta);

	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	List<TabPropostaObj> findByListCdPropostaQuery(Integer cdProposta);

	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByConclusaoTransitoNullQuery();
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtDtaSolicitacao > '2020-12-20' and t.dtDtaConclusaoTransito is not null ")
	List<TabPropostaObj> findByDtaSolicitacaoQuery();

	
}