package br.com.jtpsolution.dao.proposta;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.TabTipoCarregamentoObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabPropostaObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta")
//@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "cd_usuario")
//@NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@ManyToOne
	@JoinColumn(name = "cd_cliente")
//@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;

	@Column(name = "dt_proposta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtProposta;

	@Column(name = "tx_proposta")
//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres")
	private String txProposta;

	@ManyToOne
	@JoinColumn(name = "cd_status")
//@NotNull(message = "Status campo obrigatório!")
	private TabStatusObj tabStatusObj;

	@ManyToOne
	@JoinColumn(name = "cd_tipo_carregamento")
//@NotNull(message = "TipoCarregamento campo obrigatório!")
	private TabTipoCarregamentoObj  tabTipoCarregamentoObj;

	@ManyToOne
	@JoinColumn(name = "cd_destino")
//@NotNull(message = "Destino campo obrigatório!")
	private TabDestinoObj tabDestinoObj;

	@Column(name = "tx_lote")
//@NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 20, message = "Lote tamanho máximo de 20 caracteres")
	private String txLote;

	@Column(name = "tx_importador")
//@NotEmpty(message = "Importador campo obrigatório!")
	@Size(max = 100, message = "Importador tamanho máximo de 100 caracteres")
	private String txImportador;

	@Column(name = "tx_cnpj_importador")
//@NotEmpty(message = "CnpjImportador campo obrigatório!")
	@Size(max = 200, message = "CnpjImportador tamanho máximo de 200 caracteres")
	private String txCnpjImportador;

	@Column(name = "tx_numero_dta")
//@NotEmpty(message = "NumeroDta campo obrigatório!")
	@Size(max = 30, message = "NumeroDta tamanho máximo de 30 caracteres")
	private String txNumeroDta;


	@Column(name = "tx_bl")
//@NotEmpty(message = "Bl campo obrigatório!")
	@Size(max = 30, message = "Bl tamanho máximo de 30 caracteres")
	private String txBl;

	@Column(name = "dt_prev_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrevCarregamento;


	@Column(name = "dt_dta_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaSolicitacao;

	@Column(name = "dt_dta_registro")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaRegistro;

	@Column(name = "dt_dta_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaCarregamento;

	@Column(name = "dt_dta_parametrizacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaParametrizacao;

	@Column(name = "tx_dta_canal")
//@NotEmpty(message = "DtaCanal campo obrigatório!")
	@Size(max = 30, message = "DtaCanal tamanho máximo de 30 caracteres")
	private String txDtaCanal;

	@Column(name = "dt_dta_desembaraco")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaDesembaraco;

	@Column(name = "dt_dta_inicio_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaInicioTransito;

	@Column(name = "dt_dta_chegada_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaChegadaTransito;

	@Column(name = "dt_dta_conclusao_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaConclusaoTransito;

	@Column(name = "dt_dta_ultima_consulta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaUltimaConsulta;


	@Column(name = "tx_email_taxa_informativo")
//@NotEmpty(message = "EmailTaxaInformativo campo obrigatório!")
	@Size(max = 300, message = "EmailTaxaInformativo tamanho máximo de 300 caracteres")
	private String txEmailTaxaInformativo;

	@Column(name = "vl_taxa_informativo")
//@NotEmpty(message = "TaxaInformativo campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlTaxaInformativo;

	@Column(name = "dt_envio_email_informativo")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEnvioEmailInformativo;

	@Column(name = "dt_armazenagem")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtArmazenagem;
	
	@Column(name = "vl_cif_origem")
	//@NotEmpty(message = "CargaMoeda campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlCifOrigem;
	
	@Column(name = "vl_cif_real")
	//@NotEmpty(message = "CargaMoeda campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlCifReal;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {

	}

}
