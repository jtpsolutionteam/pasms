package br.com.jtpsolution.dao.cadastros.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.TabStatusObj;



public interface TabStatusRepository extends JpaRepository<TabStatusObj, Integer> {

	@Query("select t from TabStatusObj t where t.txTipoStatus like %?1% order by t.cdStatus")
    List<TabStatusObj> findByTxTipoStatusQuery(String txTipoStatus);

}