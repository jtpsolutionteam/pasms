package br.com.jtpsolution.dao.robos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "vw_robos_status", schema = Constants.SCHEMA)
public class VwRobosStatusObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_codigo")
	private Integer cdCodigo;

	@Column(name = "dt_robo_mantran")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtRoboMantran;

	@Column(name = "dt_robo_microled")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtRoboMicroled;

	@Column(name = "dt_robo_dta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtRoboDta;

	@Column(name = "tx_status_mantran")
//@NotEmpty(message = "StatusMantran campo obrigatório!")
	@Size(max = 9, message = "StatusMantran tamanho máximo de 9 caracteres")
	private String txStatusMantran;

	@Column(name = "tx_status_microled")
//@NotEmpty(message = "StatusMicroled campo obrigatório!")
	@Size(max = 9, message = "StatusMicroled tamanho máximo de 9 caracteres")
	private String txStatusMicroled;

	@Column(name = "tx_status_dta")
//@NotEmpty(message = "StatusDta campo obrigatório!")
	@Size(max = 9, message = "StatusDta tamanho máximo de 9 caracteres")
	private String txStatusDta;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txStatusMantran))
			txStatusMantran = txStatusMantran.toUpperCase();
		if (!Validator.isBlankOrNull(txStatusMicroled))
			txStatusMicroled = txStatusMicroled.toUpperCase();
		if (!Validator.isBlankOrNull(txStatusDta))
			txStatusDta = txStatusDta.toUpperCase();
	}

}
