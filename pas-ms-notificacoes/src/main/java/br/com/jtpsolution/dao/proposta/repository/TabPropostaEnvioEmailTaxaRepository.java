package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaEnvioEmailTaxaObj;



public interface TabPropostaEnvioEmailTaxaRepository extends JpaRepository<TabPropostaEnvioEmailTaxaObj, Integer> {

	List<TabPropostaEnvioEmailTaxaObj> findByCdProposta(Integer cdProposta);
	
	//TabPropostaEnvioEmailTaxaObj findByCdPropostaAndTxEmail(Integer cdProposta, String txEmail);
	
	@Query("select t from TabPropostaEnvioEmailTaxaObj t where t.cdPropostaEnvioEmail = ?1")
	TabPropostaEnvioEmailTaxaObj findByCdPropostaEnvioEmailQuery(Integer cdPropostaEnvioEmail);

	List<TabPropostaEnvioEmailTaxaObj> findByCdPropostaAndTxEmail(Integer cdProposta, String txEmail);
}