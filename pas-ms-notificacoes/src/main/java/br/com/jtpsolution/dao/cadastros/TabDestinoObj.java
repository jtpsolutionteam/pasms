package br.com.jtpsolution.dao.cadastros;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_destino", schema = Constants.SCHEMA)
public class TabDestinoObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_destino")
//@NotNull(message = "Destino campo obrigatório!")
	private Integer cdDestino;

	@Column(name = "cd_empresa")
//@NotNull(message = "Empresa campo obrigatório!")
	private Integer cdEmpresa;

	@Column(name = "tx_destino")
//@NotEmpty(message = "Destino campo obrigatório!")
	@Size(max = 45, message = "Destino tamanho máximo de 45 caracteres")
	private String txDestino;

	@Column(name = "vl_dta_importacao")
//@NotEmpty(message = "DtaImportacao campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlDtaImportacao;

	@Column(name = "vl_exportacao")
//@NotEmpty(message = "Exportacao campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlExportacao;

	@Column(name = "ck_rota_amalog")
//@NotNull(message = "ckRotaAmalog campo obrigatório!")
	private Integer ckRotaAmalog;

	@Column(name = "ck_ativo")
//@NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@Column(name = "cd_armazem")
//@NotNull(message = "Armazem campo obrigatório!")
	private Integer cdArmazem;

	@Column(name = "cd_codigo_ibge")
//@NotNull(message = "CodigoIbge campo obrigatório!")
	private Integer cdCodigoIbge;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDestino))
			txDestino = txDestino.toUpperCase();
	}

}
