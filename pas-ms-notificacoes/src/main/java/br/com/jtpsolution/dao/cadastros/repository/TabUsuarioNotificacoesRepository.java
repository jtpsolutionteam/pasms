package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabUsuarioNotificacoesObj;


public interface TabUsuarioNotificacoesRepository extends JpaRepository<TabUsuarioNotificacoesObj, Integer> {



}