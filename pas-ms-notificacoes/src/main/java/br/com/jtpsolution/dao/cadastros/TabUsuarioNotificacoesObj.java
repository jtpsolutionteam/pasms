package br.com.jtpsolution.dao.cadastros;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_usuario_notificacoes", schema = Constants.SCHEMA)
public class TabUsuarioNotificacoesObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_usuario_notificacoes")
//@NotNull(message = "UsuarioNotificacoes campo obrigatório!")
	private Integer cdUsuarioNotificacoes;
	
	@Column(name = "cd_usuario")
//@NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@Column(name = "cd_regra_notificacao")
//@NotNull(message = "RegraNotificacao campo obrigatório!")
	private Integer cdRegraNotificacao;

	@Column(name = "ck_ativo_notificacao")
//@NotNull(message = "ckAtivoNotificacao campo obrigatório!")
	private Integer ckAtivoNotificacao;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
