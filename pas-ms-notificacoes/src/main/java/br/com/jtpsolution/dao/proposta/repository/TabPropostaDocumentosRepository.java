package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaDocumentosObj;



public interface TabPropostaDocumentosRepository extends JpaRepository<TabPropostaDocumentosObj, Integer> {

	List<TabPropostaDocumentosObj> findByCdProposta(Integer cdProposta);

	@Query("select t from TabPropostaDocumentosObj t where t.cdProposta = ?1 and t.tabDocumentosClassificacaoObj.cdDoctoClassificacao = ?2 ")
	TabPropostaDocumentosObj findByCdPropostaAndCdClassificacaoQuery(Integer cdProposta, Integer cdClassificacao);
}