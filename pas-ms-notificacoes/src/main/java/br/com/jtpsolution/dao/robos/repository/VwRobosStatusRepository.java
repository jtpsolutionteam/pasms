package br.com.jtpsolution.dao.robos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.robos.VwRobosStatusObj;



public interface VwRobosStatusRepository extends JpaRepository<VwRobosStatusObj, Integer> {



}