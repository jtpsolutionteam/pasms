package br.com.jtpsolution.dao.cadastros;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_destino_impexp", schema = Constants.SCHEMA)
public class TabDestinoImpexpObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_destino_impexp")
//@NotNull(message = "DestinoImpexp campo obrigatório!")
	private Integer cdDestinoImpexp;

	@Column(name = "cd_destino")
//@NotNull(message = "Destino campo obrigatório!")
	private Integer cdDestino;

	@Column(name = "cd_impexp")
//@NotNull(message = "Impexp campo obrigatório!")
	private Integer cdImpexp;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
