import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.cadastros.usuarios.service.CadUsuariosService;
import br.com.jtpsolution.exceptions.ErrosException;

@Service
public class #Classe {

	@Autowired
	private #Repository  tabRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;
	
	
	public List<#TabObj> listar() {
		return tabRepository.findAll();
		
	}
	
	
	public #TabObj incluir(#TabJoinObj TabJoin) {
		
		AlcanceUser user  = tabUsuarioService.DadosUsuario();

		
		#TabObj tAtual = new #TabObj();
		#TabObj tNovo = TabJoin.get#TabObj();
		
		tNovo.set#Field(null);
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
			tabLogBeanService.LogBean(String.valueOf(tNovo.get#Field()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		
		return tNovo;
		
	}
	
	public #TabObj consultar(#TipoField #Field) {
	 #TabObj Tab = tabRepository.findOne(#Field);
	 
	 if (Tab == null) {
		 throw new ErrosException(Constants.REGISTRO_NAO_EXISTE);
	 }
	 
	   return Tab;
	 
	}
	
	public #TabObj alterar(#TabJoinObj TabJoin) {
		#TipoField #Field = TabJoin.get#TabObj().get#Field();
		AlcanceUser user  = tabUsuarioService.DadosUsuario();

		verificarExistencia(TabJoin.get#TabObj().get#Field());
		
		#TabObj tAtual = tabRepository.findOne(#Field);
		
		//Log de campos -- Sempre antes da gravação por causa da persistencia.
		List<TabLogObj> listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.get#Field()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, TabJoin.get#TabObj());

		//Gravar
		#TabObj tNovo = null;
		 try {				  
			  tNovo = tabRepository.save(TabJoin.get#TabObj());
			  tabLogBeanService.GravarListaLog(listaLog);
		  }catch (ConstraintViolationException ex) {			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }catch (TransactionSystemException e) {
			  
			 throw new ErrosTransactionSystemException(GeneralUtil.LimpaMensagemException(e.getCause().getCause().getMessage()));			  
		  }
		
			
		return tNovo;
	}

	private void verificarExistencia(#TipoField #Field) {
		consultar(#Field);
	}
	
	
	
	
	
}
