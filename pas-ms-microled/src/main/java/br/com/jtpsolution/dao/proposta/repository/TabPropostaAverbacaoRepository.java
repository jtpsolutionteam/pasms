package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaAverbacaoObj;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;



public interface TabPropostaAverbacaoRepository extends JpaRepository<TabPropostaAverbacaoObj, Integer> {


	@Query("select t from TabPropostaAverbacaoObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtAverbacao is null")
	List<TabPropostaAverbacaoObj> findByAverbacaoNullQuery();


}