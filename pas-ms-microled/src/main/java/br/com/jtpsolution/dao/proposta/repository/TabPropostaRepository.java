package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.proposta.TabPropostaObj;



public interface TabPropostaRepository extends JpaRepository<TabPropostaObj, Integer> {

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txLote is not null and t.dtAceite is not null and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByAceiteLoteConclusaoTransitoNullQuery();

	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	TabPropostaObj findByCdPropostaQuery(Integer cdProposta);
	
	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	List<TabPropostaObj> findByListCdPropostaQuery(Integer cdProposta);
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in (1,3) and t.txTipoProposta = 'DTA' and t.dtDtaRegistro is not null and t.dtDtaCarregamento is null and t.dtDoctosLiberadosRegistroDta is null")
	List<TabPropostaObj> findByListStatusMicroledQuery();
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA' and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByConclusaoTransitoNullQuery();

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA' and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtAverbacao is null and t.txDtaCanal is not null and t.txDtaCanal <> ''")
	List<TabPropostaObj> findByAverbacaoNullQuery();

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA' and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtDtaCarregamento is null")
	List<TabPropostaObj> findByDtCarregamentoNullQuery();

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA' and t.txLote is not null and t.txLote <> '' and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByDtSaidaNullQuery();

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA' and t.txNumeroCe is not null and t.txNumeroCe <> '' and (t.txLote is null or t.txLote = '') and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByTxCEQuery();

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA'and t.txBl is not null and t.txBl <> '' and (t.txNumeroCe is null or t.txNumeroCe = '') and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByTxBLQuery();

	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA'and t.txBl is not null and t.txBl <> '' and (t.dtEntradaCd is null or t.txNumeroCe is null or t.txTerminalMar <> 'Terminal')")
	List<TabPropostaObj> findByTxBLEntradaQuery();

	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and txTipoProposta = 'DTA' and t.txBl is not null and t.txBl <> '' and (t.txNumeroCe is null or t.txNumeroCe = '')")
	List<TabPropostaObj> findByTxCeArrumaQuery();

	
}