package br.com.jtpsolution.dao.proposta;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_avarias", schema = Constants.SCHEMA)
public class TabPropostaAvariasObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta_avaria")
	// @NotNull(message = "PropostaAvaria campo obrigatório!")
	private Integer cdPropostaAvaria;

	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "tx_lote")
	// @NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 10, message = "Lote tamanho máximo de 10 caracteres")
	private String txLote;

	@Column(name = "cd_item")
	// @NotNull(message = "Item campo obrigatório!")
	private Integer cdItem;

	@Column(name = "tx_embalagem")
	// @NotEmpty(message = "Embalagem campo obrigatório!")
	@Size(max = 50, message = "Embalagem tamanho máximo de 50 caracteres")
	private String txEmbalagem;

	@Column(name = "tx_termo")
	// @NotEmpty(message = "Termo campo obrigatório!")
	@Size(max = 45, message = "Termo tamanho máximo de 45 caracteres")
	private String txTermo;

	@Column(name = "dt_termo")
	// @NotEmpty(message = "Termo campo obrigatório!")
	@Size(max = 50, message = "Termo tamanho máximo de 50 caracteres")
	private String dtTermo;

	@Column(name = "tx_avaria")
	// @NotEmpty(message = "Avaria campo obrigatório!")
	@Size(max = 200, message = "Avaria tamanho máximo de 200 caracteres")
	private String txAvaria;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txLote))
			txLote = txLote.toUpperCase();
		if (!Validator.isBlankOrNull(txEmbalagem))
			txEmbalagem = txEmbalagem.toUpperCase();
		if (!Validator.isBlankOrNull(txTermo))
			txTermo = txTermo.toUpperCase();
		if (!Validator.isBlankOrNull(dtTermo))
			dtTermo = dtTermo.toUpperCase();
		if (!Validator.isBlankOrNull(txAvaria))
			txAvaria = txAvaria.toUpperCase();
	}

}
