package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.proposta.TabPropostaAvariasObj;



public interface TabPropostaAvariasRepository extends JpaRepository<TabPropostaAvariasObj, Integer> {

    List<TabPropostaAvariasObj> findByCdProposta(Integer cdProposta);

}