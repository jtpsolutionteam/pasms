package br.com.jtpsolution.dao.proposta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.proposta.TabPropostaMicroledObj;



public interface TabPropostaMicroledRepository extends JpaRepository<TabPropostaMicroledObj, Integer> {



}