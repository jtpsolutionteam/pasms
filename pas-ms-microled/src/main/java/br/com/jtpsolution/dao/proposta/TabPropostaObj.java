package br.com.jtpsolution.dao.proposta;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.TabTipoCarregamentoObj;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabPropostaObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta")
//@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "cd_usuario")
//@NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@ManyToOne
	@JoinColumn(name = "cd_cliente")
//@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;

	@Column(name = "dt_proposta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtProposta;

	@Column(name = "tx_proposta")
//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres")
	private String txProposta;

	@ManyToOne
	@JoinColumn(name = "cd_status")
//@NotNull(message = "Status campo obrigatório!")
	private TabStatusObj tabStatusObj;

	@ManyToOne
	@JoinColumn(name = "cd_tipo_carregamento")
//@NotNull(message = "TipoCarregamento campo obrigatório!")
	private TabTipoCarregamentoObj  tabTipoCarregamentoObj;

	@ManyToOne
	@JoinColumn(name = "cd_destino")
//@NotNull(message = "Destino campo obrigatório!")
	private TabDestinoObj tabDestinoObj;

	@Column(name = "vl_valor")
//@NotEmpty(message = "Valor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlValor;


	@Column(name = "tx_lote")
//@NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 20, message = "Lote tamanho máximo de 20 caracteres")
	private String txLote;

	@Column(name = "tx_cnpj_importador")
//@NotEmpty(message = "CnpjImportador campo obrigatório!")
	@Size(max = 200, message = "CnpjImportador tamanho máximo de 200 caracteres")
	private String txCnpjImportador;


	@Column(name = "vl_peso_extrato_desova")
//@NotEmpty(message = "PesoExtratoDesova campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlPesoExtratoDesova;


	@Column(name = "tx_numero_dta")
//@NotEmpty(message = "NumeroDta campo obrigatório!")
	@Size(max = 30, message = "NumeroDta tamanho máximo de 30 caracteres")
	private String txNumeroDta;

	@Column(name = "dt_dta_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtDtaSolicitacao;


	@Column(name = "tx_dta_canal")
//@NotEmpty(message = "DtaCanal campo obrigatório!")
	@Size(max = 30, message = "DtaCanal tamanho máximo de 30 caracteres")
	private String txDtaCanal;


	@Column(name = "dt_averbacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAverbacao;

	@Column(name = "tx_email_taxa_informativo")
//@NotEmpty(message = "EmailTaxaInformativo campo obrigatório!")
	@Size(max = 300, message = "EmailTaxaInformativo tamanho máximo de 300 caracteres")
	private String txEmailTaxaInformativo;

	@Column(name = "tx_numero_ce")
//@NotEmpty(message = "NumeroCe campo obrigatório!")
	@Size(max = 15, message = "NumeroCe tamanho máximo de 15 caracteres")
	private String txNumeroCe;

	@Column(name = "vl_qtde_volume")
//@NotNull(message = "QtdeVolume campo obrigatório!")
	private Integer vlQtdeVolume;
	

	@Column(name = "cd_status_averbacao")
//@NotNull(message = "StatusAverbacao campo obrigatório!")
	private Integer cdStatusAverbacao;

	@Column(name = "tx_status_averbacao")
//@NotEmpty(message = "StatusAverbacao campo obrigatório!")
	@Size(max = 300, message = "StatusAverbacao tamanho máximo de 300 caracteres")
	private String txStatusAverbacao;

	@Column(name = "tx_situacao_ce_master")
//@NotEmpty(message = "SituacaoCeMaster campo obrigatório!")
	@Size(max = 200, message = "SituacaoCeMaster tamanho máximo de 200 caracteres")
	private String txSituacaoCeMaster;

	@Column(name = "tx_status_lote_microled")
//@NotEmpty(message = "StatusLoteMicroled campo obrigatório!")
	@Size(max = 200, message = "StatusLoteMicroled tamanho máximo de 200 caracteres")
	private String txStatusLoteMicroled;

	@Column(name = "dt_ultimo_status_lote_microled")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtUltimoStatusLoteMicroled;

	@Column(name = "vl_cif_origem")
	//@NotEmpty(message = "CargaMoeda campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlCifOrigem;
	
	@Column(name = "vl_cif_real")
	//@NotEmpty(message = "CargaMoeda campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlCifReal;
	
	@Column(name = "dt_dta_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaCarregamento;
	
	@Column(name = "vl_peso_bruto") 
	//@NotEmpty(message = "PesoAvaria campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000") 
	private Double vlPesoBruto;
	
	@Column(name = "tx_volume") 
	private String txVolume;
	
	@Column(name = "tx_microled_mercadoria") 
	//@NotEmpty(message = "MicroledMercadoria campo obrigatório!")
	@Size(max = 200, message = "MicroledMercadoria tamanho máximo de 200 caracteres") 
	private String txMicroledMercadoria; 
	
	@Column(name = "tx_container") 
	//@NotEmpty(message = "Container campo obrigatório!")
	@Size(max = 20, message = "Container tamanho máximo de 20 caracteres") 
	private String txContainer; 
	
	@Column(name = "dt_inicio_desova") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtInicioDesova; 
	 
	@Column(name = "dt_fim_desova") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtFimDesova; 
	
	@Column(name = "dt_saida") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtSaida; 
	
	@Column(name = "cd_termo_avaria") 
	//@NotNull(message = "TermoAvaria campo obrigatório!")
	private Integer cdTermoAvaria; 
	 
	@Column(name = "dt_termo_avaria") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtTermoAvaria; 
	
	@Column(name = "vl_diferenca_peso") 
	//@NotEmpty(message = "DiferencaPeso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000") 
	private Double vlDiferencaPeso; 
	
	@Column(name = "vl_peso_avaria") 
	//@NotEmpty(message = "PesoAvaria campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000") 
	private Double vlPesoAvaria; 
	
	@Column(name = "dt_entrada_cd") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtEntradaCd;
	
	@Column(name = "vl_diferenca_percentual") 
	//@NotEmpty(message = "DiferencaPercentual campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00") 
	private Double vlDiferencaPercentual; 
	
	@Column(name = "dt_desconsolidacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDesconsolidacao;
	
	@Column(name = "tx_importador") 
	//@NotEmpty(message = "Importador campo obrigatório!")
	@Size(max = 200, message = "Importador tamanho máximo de 200 caracteres") 
	private String txImportador; 
	
	@Column(name = "tx_mapa") 
	//@NotEmpty(message = "Mapa campo obrigatório!")
	//@Size(max = 3, message = "Mapa tamanho máximo de 3 caracteres") 
	private String txMapa; 
	 
	@Column(name = "tx_gr_paga") 
	//@NotEmpty(message = "GrPaga campo obrigatório!")
	//@Size(max = 3, message = "GrPaga tamanho máximo de 3 caracteres") 
	private String txGrPaga; 
	 
	@Column(name = "tx_siscarga") 
	//@NotEmpty(message = "Siscarga campo obrigatório!")
	//@Size(max = 3, message = "Siscarga tamanho máximo de 3 caracteres") 
	private String txSiscarga; 
	 
	@Column(name = "tx_bloqueio_bl") 
	//@NotEmpty(message = "BloqueioBl campo obrigatório!")
	//@Size(max = 3, message = "BloqueioBl tamanho máximo de 3 caracteres") 
	private String txBloqueioBl; 
	 
	@Column(name = "tx_bloqueio_cntr") 
	//@NotEmpty(message = "BloqueioCntr campo obrigatório!")
	//@Size(max = 3, message = "BloqueioCntr tamanho máximo de 3 caracteres") 
	private String txBloqueioCntr; 
	 
	@Column(name = "tx_icms_sefaz") 
	//@NotEmpty(message = "IcmsSefaz campo obrigatório!")
	//@Size(max = 3, message = "IcmsSefaz tamanho máximo de 3 caracteres") 
	private String txIcmsSefaz;
	
	@Column(name = "dt_dta_conclusao_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaConclusaoTransito;

	
	@Column(name = "tx_tipo_proposta") 
	private String txTipoProposta;

	@Column(name = "dt_dta_registro")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaRegistro;
	
	@Column(name = "dt_doctos_liberados_registro_dta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDoctosLiberadosRegistroDta;
	
	@Column(name = "dt_aceite") 	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAceite;
	
	@Column(name = "tx_terminal_mar")
	private String txTerminalMar;
	
	@Column(name = "tx_bl")
	private String txBl;
	
	@Column(name = "dt_fma") 	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtFma;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

}
