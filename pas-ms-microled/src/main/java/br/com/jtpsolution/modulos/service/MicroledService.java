package br.com.jtpsolution.modulos.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.jtpsolution.dao.TransactionUtilBean;
import br.com.jtpsolution.dao.conexaomicroled.connectmicroled;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.proposta.TabPropostaAvariasObj;
import br.com.jtpsolution.dao.proposta.TabPropostaMicroledObj;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaAvariasRepository;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaAverbacaoRepository;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaMicroledRepository;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.obj.TabAverbacaoMicroledObj;
import br.com.jtpsolution.modulos.obj.TabStatusMicroledObj;
import br.com.jtpsolution.modulos.obj.VwBlObj;
import br.com.jtpsolution.modulos.obj.VwTabAvariaCsObj;
import br.com.jtpsolution.modulos.obj.VwTabConsultaCargaItemObj;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Component
public class MicroledService {

	@Autowired
	private TabPropostaMicroledRepository tabPropostaMicroledRepository;

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	@Autowired
	private TabPropostaAvariasRepository tabPropostaAvariasRepository;

	@Autowired
	private TabPropostaAverbacaoRepository tabPropostaAverbacaoRepository;

	@Autowired
	private TransactionUtilBean transactionUtilBean;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MicroledService c = new MicroledService();
		VwTabConsultaCargaItemObj t = c.consultaCargaItem(716213);
		System.out.println(t.getTxLote());
		System.out.println(t.getVlPesoApurado());
		System.out.println(t.getVlPesoBruto());
		System.out.println(t.getTxMapa());

		/*
		 * System.out.println(t.getTxMicroledMercadoria());
		 * 
		 * List<VwTabAvariaCsObj> tv = c.consultaAvarias(716213); for (VwTabAvariaCsObj
		 * atual : tv) {
		 * 
		 * System.out.println(atual.getTxAvaria()); }
		 */

	}
	
	/*
	@Scheduled(cron = "0 0/3 6-23 * * ?")
	private void roboatualizaCarga() {

		try {
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByTxCeArrumaQuery();

			for (TabPropostaObj atualPropostaObj : listProposta) {

				System.out.println(
						"Atualiza Lote CE Arrumar: " + atualPropostaObj.getCdProposta() + " - " + atualPropostaObj.getTxBl());

				TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

				atualizaLote(atual);

				Thread.sleep(1000);

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("roboatualizaBL :" + e.getMessage());

		}
	}
*/

	
	
	@Scheduled(cron = "0 0/5 * * * ?")
	private void roboatualizaEntrada() {

		try {
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByTxBLEntradaQuery();
			//List<TabPropostaObj> listProposta = tabPropostaRepository.findByListCdPropostaQuery(21750);


			for (TabPropostaObj atualPropostaObj : listProposta) {

				System.out.println(
						"Atualiza Lote BL Entrada: " + atualPropostaObj.getCdProposta() + " - " + atualPropostaObj.getTxBl());

				TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

				atualizaLoteEntrada(atual);

				Thread.sleep(1000);

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("roboatualizaBL :" + e.getMessage());

		}
	}


	@Scheduled(cron = "0 0/5 6-23 * * ?")
	private void roboatualizaBL() {

		try {
			//List<TabPropostaObj> listProposta = tabPropostaRepository.findByTxBLQuery();
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByListCdPropostaQuery(5457);


			for (TabPropostaObj atualPropostaObj : listProposta) {

				System.out.println(
						"Atualiza Lote BL: " + atualPropostaObj.getCdProposta() + " - " + atualPropostaObj.getTxBl());

				TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

				atualizaLote(atual);

				Thread.sleep(1000);

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("roboatualizaBL :" + e.getMessage());

		}
	}

	@Scheduled(cron = "0 0/10 6-23 * * ?")
	private void roboatualizaCE() {

		try {
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByTxCEQuery();

			for (TabPropostaObj atualPropostaObj : listProposta) {

				System.out.println("Atualiza Lote CE: " + atualPropostaObj.getCdProposta() + " - "
						+ atualPropostaObj.getTxNumeroCe());

				TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

				atualizaLote(atual);

				Thread.sleep(1000);

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("roboatualizaCE :" + e.getMessage());

		}
	}

	@Scheduled(cron = "0 0/15 * * * ?")
	private void roboatualizaLote() {

		try {
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByDtSaidaNullQuery();
			// List<TabPropostaObj> listProposta = tabPropostaRepository.findByListCdPropostaQuery(17210);

			for (TabPropostaObj atualPropostaObj : listProposta) {

				System.out.println(
						"Atualiza Lote: " + atualPropostaObj.getCdProposta() + " - " + atualPropostaObj.getTxLote());

				TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

				atualizaLote(atual);

				Thread.sleep(1000);

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("roboatualizaLote :" + e.getMessage());

		}
	}

	
	private void atualizaLoteEntrada(TabPropostaObj atual) {
		try {
			if (atual != null) {

				MicroledService microledService = new MicroledService();
				VwBlObj vwConsulta = null;

				
				if (!Validator.isBlankOrNull(atual.getTxBl())) { // BL
					vwConsulta = microledService.consultaBLEntrada(atual.getTxBl());
				}

				if (!Validator.isBlankOrNull(vwConsulta.getTxBl())) {
					atual.setTxNumeroCe(vwConsulta.getTxCe());
					atual.setDtEntradaCd(vwConsulta.getDtEntrada());
					atual.setTxTerminalMar("Terminal");
					atual = tabPropostaRepository.save(atual);
				}
			}
		} catch (Exception ex) {
			System.out.println("atualizaLote: " + ex.getMessage());
		}

	}

	
	private void atualizaLote(TabPropostaObj atual) {
		try {
			if (atual != null) {

				MicroledService microledService = new MicroledService();
				VwTabConsultaCargaItemObj vwConsulta = null;

				
				if (Validator.isBlankOrNull(atual.getTxLote()) && !Validator.isBlankOrNull(atual.getTxNumeroCe())) { // CE
					vwConsulta = microledService.consultaCargaItemCE(atual.getTxNumeroCe());
				} else if (Validator.isBlankOrNull(atual.getTxNumeroCe())
						&& !Validator.isBlankOrNull(atual.getTxBl())) { // BL
					vwConsulta = microledService.consultaCargaItemBL(atual.getTxBl());
				} else {
					vwConsulta = microledService.consultaCargaItem(GeneralParser.parseInt(atual.getTxLote()));
				}
				
				//vwConsulta = microledService.consultaCargaItem(GeneralParser.parseInt(atual.getTxLote()));

				if (!Validator.isBlankOrNull(vwConsulta.getTxLote())) {
					atual.setTxLote(String.valueOf(vwConsulta.getTxLote()));
					atual.setTxNumeroCe(vwConsulta.getTxNumeroCe());
					atual.setVlPesoBruto(vwConsulta.getVlPesoBruto());
					atual.setVlPesoExtratoDesova(new BigDecimal(vwConsulta.getVlPesoApurado()));
					atual.setVlQtdeVolume(vwConsulta.getVlQtdeVolume());
					// Tab.setVlQtdeAvaria(vwConsulta.getVlQtdeAvaria());
					atual.setTxVolume(vwConsulta.getTxVolume());
					atual.setTxMicroledMercadoria(vwConsulta.getTxMicroledMercadoria());
					atual.setTxContainer(vwConsulta.getTxContainer());
					atual.setDtInicioDesova(vwConsulta.getDtInicioDesova());
					atual.setDtFimDesova(vwConsulta.getDtFimDesova());
					atual.setDtSaida(vwConsulta.getDtSaida());
					atual.setCdTermoAvaria(vwConsulta.getCdTermoAvaria());
					atual.setDtTermoAvaria(vwConsulta.getDtTermoAvaria());					
					atual.setVlDiferencaPeso(vwConsulta.getVlDiferencaPeso());
					atual.setVlPesoAvaria(vwConsulta.getVlPesoAvaria());
					atual.setDtEntradaCd(vwConsulta.getDtEntrada());
					// Tab.setVlDiferencaPercentual(vwConsulta.getVlDiferencaPercentual());
					
					Double vlDifPercentual = GeneralParser
							.porcentagemEntreDoisValores(vwConsulta.getVlPesoApurado(), vwConsulta.getVlPesoBruto());					
					 atual.setVlDiferencaPercentual(vlDifPercentual);
					
					atual.setDtDesconsolidacao(vwConsulta.getDtDesconsolidacao());
					atual.setTxImportador(vwConsulta.getTxImportador());
					atual.setTxCnpjImportador(vwConsulta.getTxCnpjImportador());
					atual.setTxMapa(vwConsulta.getTxMapa());
					atual.setTxIcmsSefaz(vwConsulta.getTxIcmsSefaz());
					atual.setTxSiscarga(vwConsulta.getTxSiscarga());
					atual.setTxBloqueioBl(vwConsulta.getTxBloqueioBl());
					atual.setTxBloqueioCntr(vwConsulta.getTxBloqueioCNTR());
							
					if (!Validator.isBlankOrNull(vwConsulta.getTxPendencia())) {
						if (vwConsulta.getTxPendencia().equals("SIM")) {
						  atual.setTxGrPaga("NÃO");	
						}else {
						  atual.setTxGrPaga(vwConsulta.getTxGrPaga());
						} 
					}else{
						atual.setTxGrPaga(vwConsulta.getTxGrPaga());
					}
					
					
					if (Validator.isBlankOrNull(atual.getDtAverbacao()) && !Validator.isBlankOrNull(vwConsulta.getDtAverbacao())) {						
							atual.setTxStatusAverbacao("Averbação realizada manualmente via microled");						
					}

					atual.setDtAverbacao(vwConsulta.getDtAverbacao());
					atual.setTxBl(vwConsulta.getTxBl());
					atual.setTxTerminalMar("Terminal");
					
					if (!Validator.isBlankOrNull(vwConsulta.getDtFma())) {
						atual.setDtFma(GeneralParser.parseDate(vwConsulta.getDtFma()));
					}
					
					atual = tabPropostaRepository.save(atual);

					// Avaria
					incluirAvarias(atual.getCdProposta(), atual.getTxLote());

				}
			}
		} catch (Exception ex) {
			System.out.println("atualizaLote: " + ex.getMessage());
		}

	}

	// @Scheduled(cron = "0 0/20 6-23 * * ?")
	public void statusLote() {

		try {
			List<TabPropostaObj> list = tabPropostaRepository.findByListStatusMicroledQuery();

			for (TabPropostaObj atualPropostaObj : list) {

				if (!Validator.isBlankOrNull(atualPropostaObj.getTxLote())) {

					TabPropostaObj atual = tabPropostaRepository
							.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

					TabStatusMicroledObj tab = new TabStatusMicroledObj();
					tab.setLoteNumero(atual.getTxLote());

					Gson g = new GsonBuilder().disableHtmlEscaping().create();
					String txJson = g.toJson(tab);

					String json = restconnect.enviaRestPost("http://amalog.com.br:8084/valida", txJson);

					System.out.println(
							"Microled Status Lote :" + atual.getCdProposta() + " - Lote :" + atual.getTxLote());

					if (json.contains("wsvalidaSaidaCsResult")) {

						JSONObject jsonObj = new JSONObject(json);

						String txJ = jsonObj.getString("wsvalidaSaidaCsResult");

						jsonObj = new JSONObject(txJ);

						String txStatus = jsonObj.getString("message");
						atual.setTxStatusLoteMicroled(txStatus);
						atual.setDtUltimoStatusLoteMicroled(new Date());
						tabPropostaRepository.save(atual);

						System.out.println("Microled Status Lote :" + atual.getCdProposta() + " - Lote :"
								+ atual.getTxLote() + " - " + txStatus);

					}
				}

				Thread.sleep(1000);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro StatusLote: " + e.getMessage());
		}

	}

	@Scheduled(cron = "0 0/10 * * * ?")
	private void setAverbar() {

		try {

			List<TabPropostaObj> listProposta = tabPropostaRepository.findByAverbacaoNullQuery();
			String retornoMicroled = "";
			for (TabPropostaObj atualPropostaObj : listProposta) {

				TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualPropostaObj.getCdProposta());

				if (!Validator.isBlankOrNull(atual.getDtDtaSolicitacao())) {

					if (atual.getVlCifOrigem() != null) {

						if (Validator.isBlankOrNull(atual.getTxCnpjImportador())) {
							atual.setTxStatusAverbacao("Proposta sem Importador!");

						} else {

							retornoMicroled = enviarAverbacao(atual);

							if (retornoMicroled.contains("Averbacao conclu")) {
								atual.setDtAverbacao(new Date());
								atual.setTxStatusAverbacao("Averbacao concluída");
								retornoMicroled = "Averbacao concluída";
							} else {
								if (!retornoMicroled.contains("Erro envio")) {
									JSONObject j = new JSONObject(retornoMicroled);

									j = new JSONObject(j.getString("wsaverbacaoLoteResult"));

									if (!j.isNull("message")) {
										String txResposta = j.getString("message").replace("inv?lido", "inválido")
												.replace("reparti??o", "repartição");
										atual.setTxStatusAverbacao(txResposta);

										if (j.getString("message").contains("CE ja tem")) {
											atualizaLote(atual);
										}
										retornoMicroled = txResposta;
									}
								}
							}
						}

						tabPropostaRepository.save(atual);

						System.out.println(
								"Microled Averbacao: " + retornoMicroled + " - Proposta: " + atual.getCdProposta());

					}
					Thread.sleep(1000);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Averbação: " + e.getMessage());
		}

	}

	private void incluirAvarias(Integer cdProposta, String txLote) {

		try {
			// transactionUtilBean.queryInsertUpdate("delete from TabPropostaAvariasObj t
			// where t.cdProposta = " + cdProposta);
			List l = transactionUtilBean
					.querySelect("select cdProposta from TabPropostaAvariasObj where cdProposta = " + cdProposta);

			if (l != null) {
				if (l.isEmpty()) {
					MicroledService microledService = new MicroledService();
					List<VwTabAvariaCsObj> lst = microledService.consultaAvarias(GeneralParser.parseInt(txLote));

					for (VwTabAvariaCsObj atual : lst) {

						TabPropostaAvariasObj Tab = new TabPropostaAvariasObj();
						Tab.setCdItem(atual.getCdItem());
						Tab.setCdProposta(cdProposta);
						Tab.setDtTermo(atual.getDtTermoAvaria());
						Tab.setTxAvaria(atual.getTxAvaria());
						Tab.setTxEmbalagem(atual.getTxEmbalagem());
						Tab.setTxTermo(atual.getTxTermoAvaria());
						Tab.setTxLote(String.valueOf(atual.getTxLote()));
						tabPropostaAvariasRepository.save(Tab);

					}
				}
			}
		} catch (Exception ex) {
			System.out.println("Erro incluirAvarias: " + ex.getMessage());
		}
	}

	public VwTabConsultaCargaItemObj setResultSet(ResultSet rs) {

		VwTabConsultaCargaItemObj Tab = new VwTabConsultaCargaItemObj();
		try {

			double vlPesoBrutoTotal = 0;
			double vlPesoApuradoTotal = 0;
			double vlPesoAvariaTotal = 0;
			String txVolumes = "";
			Integer vlQtdeVolumes = 0;
			Integer vlQtdeVolumesAvaria = 0;
			double vlDifPesoTotal = 0;
			double vlDifPorcentTotal = 0;
			String txCnpjImportador = "";
			String txImportador = "";
			String txGrPaga = "";
			String txBloqueioBl = "";
			String txIcmsSefaz = "";
			String txMapa = "";
			String txBloqueioCNTR = "";
			String txSiscarga = "";
			String txBl = "";
			String txNumeroCe = "";
			String txPendencia = "";
			String dtFma = "";

			while (rs.next()) {

				vlPesoAvariaTotal += rs.getDouble("peso_avaria");
				vlPesoApuradoTotal += rs.getDouble("peso_apurado");
				vlPesoBrutoTotal += rs.getDouble("peso_bruto");
				vlQtdeVolumesAvaria += rs.getInt("qtde_avaria");
				vlQtdeVolumes += rs.getInt("quantidade");
				// txVolumes += vlQtdeVolumes + " " + rs.getString("embalagem") + ", ";
				vlDifPesoTotal += rs.getDouble("dif");
				vlDifPorcentTotal += rs.getDouble("difperc");
				txCnpjImportador = rs.getString("nimpcnpj");
				txImportador = rs.getString("nimportador");
				txMapa = rs.getString("mapa");
				txGrPaga = rs.getString("gr_paga");
				txBloqueioBl = rs.getString("bloqueio_bl");
				txIcmsSefaz = rs.getString("icms_sefaz");
				txBloqueioCNTR = rs.getString("bloqueio_cntr");
				txSiscarga = rs.getString("siscarga");
				txVolumes = rs.getString("obs_desova");
				txBl = rs.getString("numero");
				txNumeroCe = rs.getString("ce");
				txPendencia = rs.getString("pendencia");
				dtFma = rs.getString("dt_fma");

				Tab.setTxMicroledMercadoria(rs.getString("mercadoria"));
				Tab.setTxLote(rs.getInt("lote"));
				Tab.setVlQtdeVolume(vlQtdeVolumes);
				Tab.setTxVolume(txVolumes);
				Tab.setTxContainer(rs.getString("id_conteiner"));
				Tab.setDtInicioDesova(rs.getTimestamp("dt_inicio_desova"));
				Tab.setDtFimDesova(rs.getTimestamp("dt_fim_desova"));
				Tab.setDtSaida(rs.getTimestamp("dt_saida"));
				Tab.setCdTermoAvaria(rs.getInt("termo_avaria"));
				Tab.setDtTermoAvaria(rs.getTimestamp("dt_termo"));
				Tab.setDtAverbacao(rs.getTimestamp("data_averba"));
				Tab.setVlQtdeAvaria(vlQtdeVolumesAvaria);
				Tab.setVlPesoAvaria(vlPesoAvariaTotal);
				Tab.setVlPesoBruto(vlPesoBrutoTotal);
				Tab.setVlPesoApurado(vlPesoApuradoTotal);
				Tab.setVlDiferencaPeso(vlDifPesoTotal);
				Tab.setVlDiferencaPercentual(vlDifPorcentTotal);
				Tab.setTxCnpjImportador(txCnpjImportador);
				Tab.setTxImportador(txImportador);
				Tab.setDtInicioDesova(rs.getTimestamp("dt_inicio_desova"));
				Tab.setDtEntrada(rs.getTimestamp("dt_entrada"));
				Tab.setDtDesconsolidacao(rs.getTimestamp("dt_cadastro"));
				Tab.setTxMapa(txMapa);
				Tab.setTxBloqueioBl(txBloqueioBl);
				Tab.setTxBloqueioCNTR(txBloqueioCNTR);
				Tab.setTxIcmsSefaz(txIcmsSefaz);
				Tab.setTxSiscarga(txSiscarga);
				Tab.setTxGrPaga(txGrPaga);
				Tab.setTxBl(txBl);
				Tab.setTxNumeroCe(txNumeroCe);
				Tab.setTxPendencia(txPendencia);
				Tab.setDtFma(dtFma);

				// System.out.println(rs.getString("peso_apurado"));
			}

		} catch (Exception ex) {
			System.out.println("Erro consultaCargaItem Microled: " + ex.getMessage());
		}

		return Tab;
	}

	public VwTabConsultaCargaItemObj consultaCargaItemCE(String txNumeroCe) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		VwTabConsultaCargaItemObj Tab = new VwTabConsultaCargaItemObj();
		try {

			String StrSql = getSqlStr(true)+  "where tc.ce = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setString(1, txNumeroCe);

			ResultSet rs = prmt.executeQuery();

			Tab = setResultSet(rs);

		} catch (Exception ex) {

			System.out.println("Erro consultaCargaItem CE Microled: " + ex.getMessage());
			
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print("Erro: "+e.getMessage());
			}
		}

		return Tab;

	}

	private String getSqlStr(boolean ckProducao) {

		if (ckProducao) {
		 String StrSql = "select distinct autonum, nimpcnpj, nimportador, mercadoria, t.lote,quantidade,embalagem, t.id_conteiner,t.dt_inicio_desova, t.dt_fim_desova,t.dt_entrada,t.dt_saida,termo_avaria,"
				+ "dt_termo,qtde_avaria,peso_avaria,peso_bruto,peso_apurado,dif,difperc, dt_cadastro, gr_paga, bloqueio_bl, icms_sefaz, mapa, bloqueio_cntr, siscarga, data_averba, tc.obs_desova, tc.numero, tc.ce, tet.pendencia, dt_fma from VW_CONSULTA_CARGA_ITEM t "
				+ "left join vw_consulta_carga tc on " + "tc.lote = t.lote "
				+ "left join vw_etapas_av_bl_cntr tet on " + "tet.lote = t.lote ";
		 return StrSql;
		}else {
			 String StrSql = "select distinct autonum, nimpcnpj, nimportador, mercadoria, t.lote,quantidade,embalagem, t.id_conteiner,t.dt_inicio_desova, t.dt_fim_desova,t.dt_entrada,t.dt_saida,termo_avaria,"
						+ "dt_termo,qtde_avaria,peso_avaria,peso_bruto,peso_apurado,dif,difperc, dt_cadastro, gr_paga, bloqueio_bl, icms_sefaz, mapa, bloqueio_cntr, siscarga, data_averba, tc.obs_desova, tc.numero, tc.ce, tet.pendencia, dt_fma from VW_CONSULTA_CARGA_ITEM_PRODUCAO t "
						+ "left join vw_consulta_carga_producao tc on " + "tc.lote = t.lote "
						+ "left join vw_etapas_av_bl_cntr_producao tet on " + "tet.lote = t.lote ";
			 return StrSql;			
		}
		
	}
	
	
	public VwTabConsultaCargaItemObj consultaCargaItem(Integer cdLote) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		VwTabConsultaCargaItemObj Tab = new VwTabConsultaCargaItemObj();
		try {
			
			String StrSql = getSqlStr(true)+ " where t.lote = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setInt(1, cdLote);

			ResultSet rs = prmt.executeQuery();

			Tab = setResultSet(rs);

		} catch (Exception ex) {

			System.out.println("Erro consultaCargaItem Lote Microled: " + ex.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print("Erro: "+e.getMessage());
			}
		}

		return Tab;
	}

	public VwBlObj consultaBLEntrada(String txBl) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		VwBlObj Tab = new VwBlObj();
		try {

			String StrSql = "select autonum, numero, data_entrada, num_presenca from vw_bl where numero = ? and data_entrada is not null";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setString(1, txBl);

			ResultSet rs = prmt.executeQuery();

			while (rs.next()) {
				
				Tab.setAutonum(rs.getInt("autonum"));
				Tab.setTxBl(rs.getString("numero"));
				if (!Validator.isBlankOrNull(rs.getString("num_presenca"))) {
				  Tab.setTxCe(rs.getString("num_presenca").replace("CEMERCANTE31032008", "").trim());
				}
				Tab.setDtEntrada(rs.getTimestamp("data_entrada"));
				
			}

		} catch (Exception ex) {

			System.out.println("Erro consultaCargaItem BL Entrada Microled: " + ex.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print("Erro: "+e.getMessage());
			}
		}

		return Tab;

	}

	
	
	public VwTabConsultaCargaItemObj consultaCargaItemBL(String txBl) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		VwTabConsultaCargaItemObj Tab = new VwTabConsultaCargaItemObj();
		try {

			String StrSql = getSqlStr(true)+ " where tc.numero = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setString(1, txBl);

			ResultSet rs = prmt.executeQuery();

			Tab = setResultSet(rs);

		} catch (Exception ex) {

			System.out.println("Erro consultaCargaItem BL Microled: " + ex.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print("Erro: "+e.getMessage());
			}
		}

		return Tab;

	}

	public List<VwTabAvariaCsObj> consultaAvarias(Integer cdLote) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		List<VwTabAvariaCsObj> lst = new ArrayList<VwTabAvariaCsObj>();
		try {

			String StrSql = "select * from vw_avaria_cs where bl = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setInt(1, cdLote);

			ResultSet rs = prmt.executeQuery();

			while (rs.next()) {
				VwTabAvariaCsObj Tab = new VwTabAvariaCsObj();
				Tab.setTxLote(rs.getInt("bl"));
				Tab.setCdItem(rs.getInt("item"));
				Tab.setTxEmbalagem(rs.getString("embalagem"));
				Tab.setTxTermoAvaria(rs.getString("termo"));
				Tab.setDtTermoAvaria(rs.getString("data_termo"));
				Tab.setTxAvaria(rs.getString("avarias"));
				lst.add(Tab);
			}

		} catch (Exception ex) {

			System.out.println("Erro consultaAvarias Microled: " + ex.getMessage());
			
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print("Erro: "+e.getMessage());
			}
		}

		return lst;
	}

	public String enviarAverbacao(TabPropostaObj tabPropostaObj) {

		TabPropostaMicroledObj tabMicroled = new TabPropostaMicroledObj();

		String json = "";

		// System.out.println(fluxo.getVlMercadoriaDolar());
		// System.out.println(fluxo.getVlMercadoriaMoedaNacional());

		TabAverbacaoMicroledObj tabAverbacao = new TabAverbacaoMicroledObj();
		tabAverbacao.setCeMercante(tabPropostaObj.getTxNumeroCe());
		tabAverbacao.setCnpjImportador(GeneralUtil.TiraNaonumero(tabPropostaObj.getTxCnpjImportador()));
		tabAverbacao.setCnpjDespachante("29551937000137");
		tabAverbacao.setQuantidadeVolumes(tabPropostaObj.getVlQtdeVolume());
		tabAverbacao.setVolumeCarga(tabPropostaObj.getVlPesoExtratoDesova().doubleValue());
		tabAverbacao.setQuantidadeCntr(0);
		tabAverbacao.setCanalSiscomex(tabPropostaObj.getTxDtaCanal().replace("CANAL ", ""));
		//String[] txDta = tabPropostaObj.getTxNumeroDta().split("\\/");

		// String dtSolicitacao = GeneralParser.format_dateBR2(new
		// Date()).substring(GeneralParser.format_dateBR2(tabPropostaObj.getDtDtaSolicitacao()).length()-2,GeneralParser.format_dateBR2(tabPropostaObj.getDtDtaSolicitacao()).length());

		String txNumeroDTA = GeneralUtil.TiraNaonumero(tabPropostaObj.getTxNumeroDta());
		String txAnoDta = txNumeroDTA.substring(0, 2);
		String txNumeroDTAFinal = txNumeroDTA.substring(2, txNumeroDTA.length())+ "/20" + txAnoDta;
		
		tabAverbacao.setNumeroDocumento(txNumeroDTAFinal);
		
		/*
		if (txDta.length > 1) {
			// System.out.println(txDta[1]);
			tabAverbacao.setNumeroDocumento(GeneralUtil.TiraNaonumero(txDta[1]) + "/20" + txAnoDta);
		} else {
			// System.out.println(txDta[0]);
			tabAverbacao.setNumeroDocumento(
					GeneralUtil.TiraNaonumero(txDta[0].substring(2, txDta[0].length())) + "/20" + txAnoDta);
		}*/
		
		
		
		tabAverbacao.setTipoDocumento("DTA");
		tabAverbacao.setDataDocumento(GeneralParser.format_dateBR2(tabPropostaObj.getDtDtaSolicitacao()));
		if (tabPropostaObj.getTxSituacaoCeMaster().contains("8931364")) {
			tabAverbacao.setCodigoReparticao("8931364");
		} else if (tabPropostaObj.getTxSituacaoCeMaster().contains("8933206")) {
			tabAverbacao.setCodigoReparticao("8933206");
		} else if (tabPropostaObj.getTxSituacaoCeMaster().contains("8931319")) {
			tabAverbacao.setCodigoReparticao("8931319");
		}

		tabAverbacao.setMoedaOrigem("DOLAR");
		tabAverbacao.setValorCifOrigem(tabPropostaObj.getVlCifOrigem().doubleValue());
		tabAverbacao.setValorCifReais(tabPropostaObj.getVlCifReal().doubleValue());
		tabAverbacao.setEndEmails(tabPropostaObj.getTxEmailTaxaInformativo().replaceAll(" ", ""));

		tabAverbacao.setDestinoCarga(String.valueOf(tabPropostaObj.getTabDestinoObj().getCdCodigoIbge()));

		Gson g = new GsonBuilder().disableHtmlEscaping().create();
		String txJson = g.toJson(tabAverbacao);
		System.out.println(txJson);

		try {
			json = restconnect.enviaRestPost("http://amalog.com.br:8084/averbacao", txJson);

			System.out.println(json);

			tabMicroled.setCdProposta(tabPropostaObj.getCdProposta());
			tabMicroled.setTxJsonEnvio(txJson);
			tabMicroled.setTxJsonRetorno(json);
			tabMicroled.setDtCriacao(new Date());
			tabPropostaMicroledRepository.save(tabMicroled);

		} catch (Exception ex) {
			System.out.println("Erro JSon Averbação: " + tabPropostaObj.getCdProposta() + " - " + ex.getMessage());
		}

		return json;
	}

}
