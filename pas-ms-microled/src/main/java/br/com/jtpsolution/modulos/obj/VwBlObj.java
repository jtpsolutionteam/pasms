package br.com.jtpsolution.modulos.obj;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VwBlObj {
	
	
	private Integer autonum;
	
	private String txBl;
	
	private String txCe;
	 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtEntrada; 
	 
}
