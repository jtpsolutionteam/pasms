package br.com.jtpsolution.modulos.obj;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabAverbacaoMicroledObj {

	    private String ceMercante;
	    private String cnpjImportador;
	    private String cnpjDespachante;
	    private int quantidadeVolumes;
	    private double volumeCarga;
	    private int quantidadeCntr;
	    private String canalSiscomex;
	    private String numeroDocumento;
	    private String tipoDocumento;
	    private String dataDocumento;
	    private String codigoReparticao;
	    private String moedaOrigem;
	    private double valorCifOrigem;
	    private double valorCifReais;
	    private String endEmails;
	    private String destinoCarga;
	
	
}
