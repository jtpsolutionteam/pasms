package br.com.jtpsolution.modulos.obj;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VwTabAvariaCsObj {

	private Integer txLote;
	
	private Integer cdItem;
	
	private String txEmbalagem;
	
	private String txTermoAvaria;
	
	private String dtTermoAvaria; 

	private String txAvaria;
	
	
}
