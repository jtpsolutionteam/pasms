package br.com.jtpsolution.modulos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.jtpsolution.dao.TransactionUtilBean;

@Controller
@RequestMapping("/relatorios")
public class RelatoriosController {

	@Autowired 
	private TransactionUtilBean transactionUtilBean;
	
	
	@GetMapping
	public @ResponseBody List<?> listarDadosRelatorio() {
		
		String StrSql = "select txProposta, dtProposta from TabPropostaObj where cdProposta in(18532, 18533)";
		
		List l = transactionUtilBean.querySelect(StrSql);
		
		
		return l;
		
		
	}
	
	
}
