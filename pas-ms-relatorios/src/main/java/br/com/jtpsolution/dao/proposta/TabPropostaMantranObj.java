package br.com.jtpsolution.dao.proposta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_mantran", schema = Constants.SCHEMA)
public class TabPropostaMantranObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_codigo")
	// @NotNull(message = "Codigo campo obrigatório!")
	private Integer cdCodigo;

	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "tx_json_envio")
	// @NotEmpty(message = "JsonEnvio campo obrigatório!")
	@Size(max = 2147483647, message = "JsonEnvio tamanho máximo de 2147483647 caracteres")
	private String txJsonEnvio;

	@Column(name = "tx_json_retorno")
	// @NotEmpty(message = "JsonRetorno campo obrigatório!")
	@Size(max = 2147483647, message = "JsonRetorno tamanho máximo de 2147483647 caracteres")
	private String txJsonRetorno;

	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCriacao;
	
	@Column(name= "ck_erro")
	private Integer ckErro;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}

}
