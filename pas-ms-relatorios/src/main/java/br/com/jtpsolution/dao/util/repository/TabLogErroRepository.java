package br.com.jtpsolution.dao.util.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.util.TabLogErroObj;



public interface TabLogErroRepository extends JpaRepository<TabLogErroObj, Integer> {



}