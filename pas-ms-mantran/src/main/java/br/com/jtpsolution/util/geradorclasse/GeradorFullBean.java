package br.com.jtpsolution.util.geradorclasse;

import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.geradorcontroller.GeradorControllerBean;
import br.com.jtpsolution.util.geradorhtml.GeradorHTMLBean;
import br.com.jtpsolution.util.geradorservice.GeradorServiceBean;

public class GeradorFullBean {

	public static void main(String[] args) {
		
		
		String listaOBJ = "TabMenuGrupoVisaoObj";
		String nomeTelaHtml = "documentosclassificacao";		
		Integer cd_tipo_tela_html = 5; // 1 - Normal; 2 - Filho; 3 - Pesquisa; 4 - Filho Modal; 5 - Pesquisa/Gravação;  6 - Pesquisa/Relatório
		
		new GeneralUtil().ApagarArquivosDiretorioGeradores();		
		new GeradorServiceBean().GeraListaService(listaOBJ);
		new GeradorControllerBean().GeraListaController(listaOBJ, cd_tipo_tela_html);		
		new GeradorHTMLBean().GeraListaHTML(listaOBJ, nomeTelaHtml, cd_tipo_tela_html);
		System.out.println("GeradorFull terminado!");
		
		
	}
	
	
}
