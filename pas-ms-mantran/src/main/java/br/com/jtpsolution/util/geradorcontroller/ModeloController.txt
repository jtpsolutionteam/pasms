
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;

@Controller
@RequestMapping("/#TabObj")
public class #Classe {

	@Autowired
	private #ServiceService tabService;
	
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;

 	@GetMapping	
	public @ResponseBody List<#TabObj> pesquisa(#TabFiltroObj #tabFiltroObj) {
		
		
		return  retornoFiltro(#tabFiltroObj);
	}
	
	private List<#TabObj> retornoFiltro(#TabFiltroObj #tabFiltroObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(#TabObj.class);
		
		adicionarFiltro(#tabFiltroObj, criteria);
 		
 		return criteria.list();
 	}


	
	private void adicionarFiltro(#TabFiltroObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getTxNomeFiltro())) {
				  criteria.add(Restrictions.eq("txNome", filtro.getTxNomeFiltro()));
				  ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("txNome", "1"));
			  }
			  
			  criteria.addOrder(Order.asc("txNome"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public @ResponseBody List<?> gravar(@RequestBody @Validated #TabObj #tabObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {		  
		  error.addAll(erros.getAllErrors());
		  return error;
		}/*else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}*/

	    #TabObj Tab = tabService.gravar(#tabObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	

	@RequestMapping(value = "/consultar/{#Field}", method = RequestMethod.GET)
	public @ResponseBody #TabObj consultar(@PathVariable #TipoField #Field, HttpServletRequest request) {
	
	   #TabObj TabView = tabService.consultar(#Field);
		
		return TabView;
		
	}
	
	/*
	@RequestMapping(value = "/excluir/{#Field}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable #TipoField #Field, HttpServletRequest request) {

		tabService.excluir(CdUsuario);

		return "OK!";
	}*/
	
}
	
	