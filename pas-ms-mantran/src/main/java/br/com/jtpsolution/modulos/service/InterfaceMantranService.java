package br.com.jtpsolution.modulos.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.proposta.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.proposta.TabPropostaMantranObj;
import br.com.jtpsolution.dao.proposta.TabPropostaObj;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaMantranRepository;
import br.com.jtpsolution.dao.proposta.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.obj.TabMantranCteObj;
import br.com.jtpsolution.modulos.obj.TabMantranDoctosObj;
import br.com.jtpsolution.modulos.obj.TabMantranDtaObj;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Service
public class InterfaceMantranService {

	@Autowired
	private TabPropostaMantranRepository tabPropostaMantranRepository;

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	@Scheduled(cron = "0 0/10 * * * ?")
	public void enviaMantranConclusaoTransito() {

		try {

			Timestamp dt = GeneralParser.parseTimestamp(GeneralParser.retornaDataAnteriorDiasCorridos(new Date(), 40));

			List<TabPropostaObj> listProposta = tabPropostaRepository.findByConclusaoTransitoQuery(dt);
			// List<TabPropostaObj> listProposta =
			// tabPropostaRepository.findByCdPropostaQuery(10431);

			for (TabPropostaObj atualProposta : listProposta) {

				if (!Validator.isBlankOrNull(atualProposta.getTxLote())) {

					System.out.println("Inicio envio Mantran Conclusão: " + atualProposta.getCdProposta());

					TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualProposta.getCdProposta());

					String txEnvioMantran = InterfaceMantranDTA(atual);
					System.out.println("json DTA Mantran: " + txEnvioMantran);
					if (!txEnvioMantran.contains("Lote_Filhote")) {
						if (txEnvioMantran.contains("ID")) {
							JSONObject json = new JSONObject(txEnvioMantran);

							if (!json.isNull("ID")) {
								if (json.getInt("ID") > 0) {
									atual.setCdIdMantran(json.getInt("ID"));
								}
								atual.setCkDtaConclusaoTransito(1);

							} else if (!json.isNull("MENSAGEM_ERRO")) {
								atual.setTxErroMantran(json.getString("MENSAGEM_ERRO"));
								atual.setCkDtaConclusaoTransito(0);								
							} else {
								atual.setTxErroMantran("Erro resquest DTA Mantran");
								atual.setCkDtaConclusaoTransito(0);
							}

							tabPropostaRepository.save(atual);

						}
					}
				}

				Thread.sleep(1000);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Interface Mantran");
		}
	}

	@Scheduled(cron = "0 0/15 * * * ?")
	public void enviaMantran() {

		try {

			List<TabPropostaObj> listProposta = tabPropostaRepository.findByAceiteLoteConclusaoTransitoNullQuery();
			//List<TabPropostaObj> listProposta = tabPropostaRepository.findByListCdPropostaQuery(15800);

			for (TabPropostaObj atualProposta : listProposta) {

				if (!Validator.isBlankOrNull(atualProposta.getTxLote())) {

					System.out.println("Inicio envio Mantran: " + atualProposta.getCdProposta());

					TabPropostaObj atual = tabPropostaRepository.findByCdPropostaQuery(atualProposta.getCdProposta());

					String txEnvioMantran = InterfaceMantranDTA(atual);
					System.out.println("json DTA Mantran: " + txEnvioMantran);
					if (!txEnvioMantran.contains("Lote_Filhote")) {
						if (txEnvioMantran.contains("ID")) {
							JSONObject json = new JSONObject(txEnvioMantran);

							if (!json.isNull("ID")) {

								if (json.getInt("ID") > 0) {
									atual.setCdIdMantran(json.getInt("ID"));
								}
								atual.setCkDtaConclusaoTransito(0);

							} else if (!json.isNull("MENSAGEM_ERRO")) {
								atual.setTxErroMantran(json.getString("MENSAGEM_ERRO"));
								atual.setCkDtaConclusaoTransito(0);
							} else {
								atual.setTxErroMantran("Erro resquest DTA Mantran");
								atual.setCkDtaConclusaoTransito(0);
							}

							tabPropostaRepository.save(atual);

						}
					}
				}

				Thread.sleep(1000);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Interface Mantran");
		}
	}

	/*
	 * private boolean atualizaLote(Integer cdProposta) {
	 * 
	 * try { TabPropostaObj Tab = tabPropostaService.consultarSimple(cdProposta);
	 * 
	 * if (Tab != null) {
	 * 
	 * MicroledService microledService = new MicroledService();
	 * VwTabConsultaCargaItemObj vwConsulta = microledService
	 * .consultaCargaItem(GeneralParser.parseInt(Tab.getTxLote()));
	 * 
	 * if (!Validator.isBlankOrNull(vwConsulta.getTxLote())) {
	 * Tab.setVlPesoBruto(vwConsulta.getVlPesoBruto());
	 * Tab.setVlPesoExtratoDesova(vwConsulta.getVlPesoApurado());
	 * Tab.setVlQtdeVolume(vwConsulta.getVlQtdeVolume()); //
	 * Tab.setVlQtdeAvaria(vwConsulta.getVlQtdeAvaria());
	 * Tab.setTxVolume(vwConsulta.getTxVolume());
	 * Tab.setTxMicroledMercadoria(vwConsulta.getTxMicroledMercadoria());
	 * Tab.setTxContainer(vwConsulta.getTxContainer());
	 * Tab.setDtInicioDesova(vwConsulta.getDtInicioDesova());
	 * Tab.setDtFimDesova(vwConsulta.getDtFimDesova());
	 * Tab.setDtSaida(vwConsulta.getDtSaida());
	 * Tab.setCdTermoAvaria(vwConsulta.getCdTermoAvaria());
	 * Tab.setDtTermoAvaria(vwConsulta.getDtTermoAvaria());
	 * Tab.setVlDiferencaPeso(vwConsulta.getVlDiferencaPeso());
	 * Tab.setVlPesoAvaria(vwConsulta.getVlPesoAvaria());
	 * Tab.setDtEntradaCd(vwConsulta.getDtEntrada()); //
	 * Tab.setVlDiferencaPercentual(vwConsulta.getVlDiferencaPercentual());
	 * Tab.setVlDiferencaPercentual(GeneralParser.porcentagemEntreDoisValores(
	 * vwConsulta.getVlPesoApurado(), vwConsulta.getVlPesoBruto()));
	 * Tab.setDtDesconsolidacao(vwConsulta.getDtDesconsolidacao());
	 * Tab.setTxImportador(vwConsulta.getTxImportador());
	 * Tab.setTxCnpjImportador(vwConsulta.getTxCnpjImportador());
	 * Tab.setTxMapa(vwConsulta.getTxMapa());
	 * Tab.setTxIcmsSefaz(vwConsulta.getTxIcmsSefaz());
	 * Tab.setTxSiscarga(vwConsulta.getTxSiscarga());
	 * Tab.setTxBloqueioBl(vwConsulta.getTxBloqueioBl());
	 * Tab.setTxBloqueioCntr(vwConsulta.getTxBloqueioCNTR());
	 * Tab.setTxGrPaga(vwConsulta.getTxGrPaga());
	 * 
	 * tabPropostaService.gravar(Tab); return true; } } } catch (Exception e) { //
	 * TODO: handle exception System.out.println(e.getMessage()); return false; }
	 * 
	 * return false; }
	 * 
	 */
	public String InterfaceMantranDTA(TabPropostaObj tabProposta) {

		String txRetorno = "";
		String txJson = "";
		try {

			if (tabProposta != null) {

				TabMantranDtaObj tM1 = new TabMantranDtaObj();

				if (!Validator.isBlankOrNull(tabProposta.getCdIdMantran())) {
					if (tabProposta.getCdIdMantran() > 0) {
						tM1.setID(tabProposta.getCdIdMantran());
					}
				}

				// tM1.setKey("jhg4j56hgK7JH9S0"); //Teste
				tM1.setKey("028kj86hgK7JH9S0");
				// tM1.setID("");
				tM1.setBL_Filhote(tabProposta.getTxBl());
				tM1.setLote_Filhote(tabProposta.getTxLote());
				tM1.setDT_Solicitacao_DTA(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaSolicitacao()));
				tM1.setDestino_Carga(tabProposta.getTabDestinoObj().getTxDestino());

				if (tabProposta.getTxCnpjImportador() != null) {
					if (tabProposta.getTxCnpjImportador().length() > 14) {
						tM1.setCD_Importador(
								GeneralUtil.TiraNaonumero(tabProposta.getTxCnpjImportador()).substring(0, 14));
					} else {
						tM1.setCD_Importador(GeneralUtil.TiraNaonumero(tabProposta.getTxCnpjImportador()));
					}
				}
				tM1.setCD_Tipo_DTA(GeneralUtil.setFormatZeroEsq(
						String.valueOf(tabProposta.getTabTipoCarregamentoObj().getCdTipoCarregamento()), 2));
				tM1.setNR_DTA(tabProposta.getTxNumeroDta());
				tM1.setCanal(tabProposta.getTxDtaCanal());
				tM1.setDT_Registro_DTA(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaRegistro()));
				tM1.setDT_Parametrizacao_DTA(
						GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaParametrizacao()));
				tM1.setDT_Inicio_Transito(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaInicioTransito()));
				tM1.setDT_Chegada_Destino(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaChegadaTransito()));
				tM1.setDT_Conclusao_Transito(
						GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaConclusaoTransito()));
				String txMercadoria = tabProposta.getTxMercadoria();
				if (txMercadoria.length() > 100) {
					txMercadoria = txMercadoria.substring(0, 95) + "...";
					tM1.setMercadoria(txMercadoria);
				} else {
					tM1.setMercadoria(txMercadoria);
				}
				tM1.setVR_Cubagem(tabProposta.getVlM3Pack() != null ? tabProposta.getVlM3Pack().floatValue() : null);
				tM1.setVR_Peso(
						tabProposta.getVlPesoExtratoDesova() != null ? tabProposta.getVlPesoExtratoDesova().floatValue()
								: null);
				tM1.setVR_Volumes(
						tabProposta.getVlQtdeVolume() != null ? tabProposta.getVlQtdeVolume().floatValue() : null);
				tM1.setVR_Mercadoria(
						tabProposta.getVlMercadoriaInvoice() != null ? tabProposta.getVlMercadoriaInvoice().floatValue()
								: null);
				tM1.setFL_Hubport_Inativo(null);
				tM1.setFL_Bloqueio(null);
				String txNavio = tabProposta.getTxNavio() + "/" + tabProposta.getTxViagem();
				if (txNavio.length() > 50) {
					tM1.setNavio_Viagem(txNavio.substring(0, 49));
				} else {
					tM1.setNavio_Viagem(txNavio);
				}
				tM1.setDT_Indicacao_DTA(null);
				tM1.setDT_Cadastro(null);
				tM1.setNR_Container(tabProposta.getTxContainer());
				tM1.setDT_Entrada_Container(GeneralParser.format_datetimeBRMySql(tabProposta.getDtEntradaCd()));
				tM1.setDT_Desconsolidacao(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDesconsolidacao()));
				tM1.setDT_Inicio_Desova(GeneralParser.format_datetimeBRMySql(tabProposta.getDtInicioDesova()));
				tM1.setDT_Desova(GeneralParser.format_datetimeBRMySql(tabProposta.getDtFimDesova()));
				tM1.setDT_Inicio_Carregamento(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaCarregamento()));

				// TabClienteObj tabCliente =
				// tabClienteService.consultar(tabProposta.getCdCliente());
				tM1.setCD_Indicador_NVOCC(GeneralUtil.TiraNaonumero(tabProposta.getTabClienteObj().getTxCnpj()));

				if (!Validator.isBlankOrNull(tabProposta.getTabColoaderDespachanteObj())) {
					// tabCliente =
					// tabClienteService.consultar(tabProposta.getTabColoaderDespachanteObj().getCdCliente());
					tM1.setCD_Coloader(
							GeneralUtil.TiraNaonumero(tabProposta.getTabColoaderDespachanteObj().getTxCnpj()));
				} else {
					tM1.setCD_Coloader(null);
				}

				tM1.setFL_Hubport_Freehand(null);
				tM1.setFL_RO(null);
				tM1.setFL_FMA(null);
				if (tabProposta.getCkImoPerigoso() != null) {
					tM1.setCD_IMO(tabProposta.getCkImoPerigoso() == 1 ? "1" : "0"); // (0, 1, 2 onde 0=ONU 1=CLASSE
																					// 2-RISCO)
				} else {
					tM1.setCD_IMO("0");
				}
				if (tabProposta.getCkAnvisa() != null) {
					tM1.setFL_Anvisa(tabProposta.getCkAnvisa() == 1 ? "S" : "N");
				} else {
					tM1.setFL_Anvisa("N");
				}
				tM1.setDT_Averbacao(GeneralParser.format_datetimeBRMySql(tabProposta.getDtAverbacao()));
				tM1.setFL_Averbou(tabProposta.getDtAverbacao() != null ? "S" : "N");
				tM1.setDT_Saida_Terminal(GeneralParser.format_datetimeBRMySql(tabProposta.getDtSaida()));
				if (!Validator.isBlankOrNull(tabProposta.getTxIcmsSefaz())) {
					tM1.setFL_Icms_Sefaz(tabProposta.getTxIcmsSefaz().equals("SIM") ? "S" : "N");
				} else {
					tM1.setFL_Icms_Sefaz("N");
				}
				tM1.setFL_Desembaracada(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaDesembaraco()));
				if (!Validator.isBlankOrNull(tabProposta.getTxMapa())) {
					tM1.setFL_Mapa_Madeira(tabProposta.getTxMapa().equals("SIM") ? "S" : "N");
				} else {
					tM1.setFL_Mapa_Madeira("N");
				}
				if (!Validator.isBlankOrNull(tabProposta.getTxGrPaga())) {
					tM1.setFL_GR_Paga(tabProposta.getTxGrPaga().equals("SIM") ? "S" : "N");
				} else {
					tM1.setFL_GR_Paga("N");
				}
				if (!Validator.isBlankOrNull(tabProposta.getTxSiscarga())) {
					tM1.setFL_SistCarga(tabProposta.getTxSiscarga().equals("SIM") ? "S" : "N");
				} else {
					tM1.setFL_SistCarga("N");
				}
				if (!Validator.isBlankOrNull(tabProposta.getTxBloqueioBl())) {
					tM1.setFL_Bloqueio_BL(tabProposta.getTxBloqueioBl().equals("SIM") ? "S" : "N");
				} else {
					tM1.setFL_Bloqueio_BL("N");
				}
				tM1.setUF_Destino(tabProposta.getTabDestinoObj().getTxUf());
				tM1.setNR_FMA(null);
				tM1.setCD_Expedidor(null);
				tM1.setDS_VOLUME(tabProposta.getTxVolume());
				tM1.setNR_PROPOSTA_PAS(tabProposta.getTxProposta());
				tM1.setE_Mail_Faturamento(tabProposta.getTxEmailTaxaInformativo());
				tM1.setVR_Peso_Bruto(
						tabProposta.getVlPesoBruto() != null ? tabProposta.getVlPesoBruto().floatValue() : null);
				tM1.setVR_Diferenca_Peso(
						tabProposta.getVlDiferencaPeso() != null ? tabProposta.getVlDiferencaPeso().floatValue()
								: null);
				tM1.setVR_Diferenca_Peso_Percent(tabProposta.getVlDiferencaPercentual() != null
						? tabProposta.getVlDiferencaPercentual().floatValue()
						: null);

				if (tabProposta.getCkFreteExclusivo() != null) {
					tM1.setFL_Dedicado(tabProposta.getCkFreteExclusivo() == 1 ? "S" : "N");
				}

				if (tabProposta.getCkNaoRemonte() != null) {
					tM1.setFL_Empilhavel(tabProposta.getCkNaoRemonte() == 1 ? "S" : "N");
				} else {
					tM1.setFL_Empilhavel("S");
				}

				tM1.setUltimo_Status(tabProposta.getTabStatusObj().getTxStatus());
				tM1.setDT_Prev_Carregamento(GeneralParser.format_datetimeBRMySql(tabProposta.getDtPrevCarregamento()));

				if (!Validator.isBlankOrNull(tabProposta.getTxDtaCanal())) {
					if (tabProposta.getTxDtaCanal().toUpperCase().contains("VERDE")) {
						tM1.setFL_Canal_Verde("S");
					}
				}
				if (!Validator.isBlankOrNull(tabProposta.getTxSituacaoCeMaster())) {
					if (tabProposta.getTxSituacaoCeMaster().contains("8933206")) {
						tM1.setCD_Expedidor("58188756002210");
					} else if (tabProposta.getTxSituacaoCeMaster().contains("8931364")) {
						tM1.setCD_Expedidor("58188756000277");
					} else if (tabProposta.getTxSituacaoCeMaster().contains("8932759")) {
						tM1.setCD_Expedidor("58188756001249");
					}

				}

				// List<TabPropostaDocumentosObj> lDoctos =
				// tabPropostaService.listaDocumentos(cdProposta);
				List<TabPropostaDocumentosObj> lDoctos = tabProposta.getListaDocumentos();

				List<TabMantranDoctosObj> lMantranDoctos = new ArrayList<TabMantranDoctosObj>();
				for (TabPropostaDocumentosObj atual : lDoctos) {

					TabMantranDoctosObj tDoctos = new TabMantranDoctosObj();
					String txUrl = atual.getTxUrl().replace("/proposta/", "https://amalog.com.br/documentos/");
					tDoctos.setNome_Completo_Documento(txUrl);
					if (!Validator.isBlankOrNull(atual.getTabDocumentosClassificacaoObj())) {
						tDoctos.setCD_Classificacao(
								String.valueOf(atual.getTabDocumentosClassificacaoObj().getCdDoctoClassificacao()));
						tDoctos.setDS_Classificacao(atual.getTabDocumentosClassificacaoObj().getTxDoctoClassificacao());
					} else {
						tDoctos.setCD_Classificacao(null);
						tDoctos.setDS_Classificacao(null);
					}

					tDoctos.setDT_Upload(GeneralParser.format_datetimeBRMySql(atual.getDtCriacao()));
					tDoctos.setDT_Validacao(GeneralParser.format_datetimeBRMySql(atual.getDtValidacao()));
					tDoctos.setID_Arquivo_PAS(atual.getCdPropostaDocumento());
					tDoctos.setObservacao(atual.getTxObs());
					tDoctos.setPendencia(atual.getTxPendencia());
					if (!Validator.isBlankOrNull(atual.getTabUsuarioCriacaoObj())) {
						tDoctos.setUsuario_Upload(atual.getTabUsuarioCriacaoObj().getTxApelido());
					} else {
						tDoctos.setUsuario_Upload(null);
					}
					if (!Validator.isBlankOrNull(atual.getTabUsuarioValidacaoObj())) {
						tDoctos.setUsuario_Validacao(atual.getTabUsuarioValidacaoObj().getTxApelido());
					} else {
						tDoctos.setUsuario_Validacao(null);
					}
					lMantranDoctos.add(tDoctos);
				}

				tM1.setArquivos(lMantranDoctos);

				Gson g = new GsonBuilder().disableHtmlEscaping().create();
				txJson = g.toJson(tM1);
				// System.out.println(txJson);

				if (Validator.isBlankOrNull(tabProposta.getCdIdMantran())) {
					txRetorno = restconnect.enviaRestPost("http://189.108.172.20:91/api/dta", txJson);
					System.out.println("Mantran DTA POST: " + tabProposta.getCdProposta());
				} else if (tabProposta.getCdIdMantran() == 0) {
					txRetorno = restconnect.enviaRestPost("http://189.108.172.20:91/api/dta", txJson);
					System.out.println("Mantran DTA POST: " + tabProposta.getCdProposta());
				} else {
					if (tabProposta.getCdIdMantran() > 0) {
						txRetorno = restconnect.enviaRestPut(
								"http://189.108.172.20:91/api/dta/" + tabProposta.getCdIdMantran(), txJson);
						System.out.println("Mantran DTA PUT: " + tabProposta.getCdProposta() + " - ID:"
								+ tabProposta.getCdIdMantran());
					}
				}

				if (txRetorno.contains("existe um DTA para com mesmo")
						|| txRetorno.contains("mas apenas alterado (PUT)")) {

					txRetorno = restconnect.enviaRestGet(
							"http://189.108.172.20:91/api/dta/028kj86hgK7JH9S0/bl_filhote/" + tabProposta.getTxBl());

					if (!txRetorno.contains("Lote_Filhote")) {
						JSONObject json = new JSONObject(txRetorno);
						if (!json.isNull("ID")) {

							Integer id = json.getInt("ID");
							System.out.println("Mantran DTA GET: " + tabProposta.getCdProposta() + " - ID:" + id);

							txRetorno = restconnect.enviaRestPut("http://189.108.172.20:91/api/dta/" + id, txJson);
							System.out.println("Mantran DTA PUT: " + tabProposta.getCdProposta() + " - ID:" + id);
						}
					}
				}

				// Inicio - Gravar log Mantran
				gravaLogMantran(tabProposta.getCdProposta(), txJson, txRetorno);
				// Fim - Gravar log Mantran

			}

		} catch (Exception ex) {
			// ex.printStackTrace();
			String txErro = "Erro request DTA Mantran: " + ex.getMessage() + " - " + tabProposta.getCdProposta();
			System.out.println("Erro request DTA Mantran: " + ex.getMessage() + " - " + txErro);
			System.out.println("Json Enviado: " + txJson);
			gravaLogMantran(tabProposta.getCdProposta(), txJson, txErro);
		}
		return txRetorno;

	}

	private void gravaLogMantran(Integer cdProposta, String txJsonEnvio, String txJsonRetorno) {

		JSONObject json = new JSONObject(txJsonRetorno);
		
		TabPropostaMantranObj tab = new TabPropostaMantranObj();
		if (!json.isEmpty()) {			
			if (!json.isNull("MENSAGEM_ERRO")) {
				if (!Validator.isBlankOrNull(json.getString("MENSAGEM_ERRO"))) {
				  tab.setCkErro(1);
				}else {
				  tab.setCkErro(2);
				}
			}else {
				tab.setCkErro(1);
			}
		}
		
		tab.setCdProposta(cdProposta);
		tab.setTxJsonEnvio(txJsonEnvio);
		tab.setTxJsonRetorno(txJsonRetorno);
		tab.setDtCriacao(new Date());
		tabPropostaMantranRepository.save(tab);

	}
	
	

	public String InterfaceMantranCTE(Integer cdProposta) {

		String txJson = "";

		TabPropostaObj tabProposta = tabPropostaRepository.findByCdPropostaQuery(cdProposta);

		if (tabProposta != null) {

			TabMantranCteObj tM1 = new TabMantranCteObj();
			tM1.setKey("028kj86hgK7JH9S0");
			tM1.setCGC_CPF_Remetente(tabProposta.getTxCnpjImportador());
			tM1.setRazao_Social_Remetente(tabProposta.getTxImportador());				
			
			tM1.setNR_NF(null);
			tM1.setCGC_CPF_Cliente(tabProposta.getTxCnpj());
			tM1.setRazao_Social_Cliente(tabProposta.getTxNomePagador());
			
			tM1.setCGC_CPF_Destinatario(null);
			tM1.setIE_Destinatario(null);		
			
			tM1.setRazao_Social_Destinatario(tabProposta.getTabDestinoObj().getTxNome());
			tM1.setEndereco_Entrega(tabProposta.getTabDestinoObj().getTxEndereco());
			tM1.setCidade_Entrega(tabProposta.getTabDestinoObj().getTxCidade());
			tM1.setUF_Entrega(tabProposta.getTabDestinoObj().getTxUf());
			tM1.setCEP_Destinatario(tabProposta.getTabDestinoObj().getTxCep());
			tM1.setCEP_REF_Destino(null);

			
			tM1.setCGC_CPF_Faturamento(tabProposta.getTxCnpj());
			tM1.setRazao_Social_Faturamento(tabProposta.getTxNomePagador());
			
			tM1.setUF_Origem("SP");
			tM1.setCEP_Origem(null);
			
			tM1.setCEP_REF_Origem(null);
			tM1.setDT_Emissao(GeneralParser.format_dateBR2(tabProposta.getDtDtaSolicitacao()));
			tM1.setID_Volume(null);
			tM1.setQT_Volume(tabProposta.getVlQtdeVolume());
			tM1.setPeso_NF(tabProposta.getVlPesoExtratoDesova().floatValue());
			tM1.setCubagem(tabProposta.getVlM3Pack().floatValue());
			tM1.setPeso_Calculo(null);
			tM1.setVR_Total_NF(null);
			tM1.setBairro_Entrega(null);
			tM1.setNR_DI(tabProposta.getTxNumeroDta());

			

			Gson g = new GsonBuilder().disableHtmlEscaping().create();
			txJson = g.toJson(tM1);
			System.out.println(txJson);

			// 177.70.122.154:39533

		}

		return txJson;

	}

}
