package br.com.jtpsolution.dao.proposta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.TabUsuarioObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_documentos", schema = Constants.SCHEMA)
public class TabPropostaDocumentosObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta_documento")
	// @NotNull(message = "PropostaDocumento campo obrigatório!")
	private Integer cdPropostaDocumento;

	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "tx_nome_arquivo")
	// @NotEmpty(message = "NomeArquivo campo obrigatório!")
	@Size(max = 200, message = "NomeArquivo tamanho máximo de 200 caracteres")
	private String txNomeArquivo;

	@Column(name = "tx_url")
	// @NotEmpty(message = "Url campo obrigatório!")
	@Size(max = 300, message = "Url tamanho máximo de 200 caracteres")
	private String txUrl;

	@ManyToOne
	@JoinColumn(name = "cd_classificacao")
	// @NotNull(message = "Classificacao campo obrigatório!")
	private TabDocumentosClassificacaoObj tabDocumentosClassificacaoObj;
	
	@Column(name = "cd_validado")
	private Integer cdValidado;

	@Column(name = "tx_pendencia")
	private String txPendencia;

	
	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCriacao;
	
	@Column(name = "dt_validacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtValidacao;

	@ManyToOne
	@JoinColumn(name = "cd_usuario_criacao")
	private TabUsuarioObj tabUsuarioCriacaoObj;
	
	@ManyToOne
	@JoinColumn(name = "cd_usuario_validacao")
	private TabUsuarioObj tabUsuarioValidacaoObj;
	
	@Column(name = "tx_obs")
	private String txObs;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}



}
