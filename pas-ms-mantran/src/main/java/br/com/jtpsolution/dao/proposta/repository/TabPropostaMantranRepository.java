package br.com.jtpsolution.dao.proposta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.proposta.TabPropostaMantranObj;



public interface TabPropostaMantranRepository extends JpaRepository<TabPropostaMantranObj, Integer> {



}