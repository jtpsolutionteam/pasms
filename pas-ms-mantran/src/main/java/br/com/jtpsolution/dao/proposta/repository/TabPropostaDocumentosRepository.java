package br.com.jtpsolution.dao.proposta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.proposta.TabPropostaDocumentosObj;



public interface TabPropostaDocumentosRepository extends JpaRepository<TabPropostaDocumentosObj, Integer> {

	List<TabPropostaDocumentosObj> findByCdProposta(Integer cdProposta);

	
}