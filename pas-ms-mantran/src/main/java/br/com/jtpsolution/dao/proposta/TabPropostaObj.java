package br.com.jtpsolution.dao.proposta;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.TabTipoCarregamentoObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabPropostaObj {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_proposta")
//@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "cd_usuario")
//@NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@ManyToOne
	@JoinColumn(name = "cd_cliente")
//@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;

	@Column(name = "dt_proposta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtProposta;
	
	@Column(name = "tx_tipo_proposta") 
	private String txTipoProposta; 
	
	@Column(name = "dt_aceite") 	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAceite;

	@Column(name = "tx_proposta")
//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres")
	private String txProposta;

	@ManyToOne
	@JoinColumn(name = "cd_status")
//@NotNull(message = "Status campo obrigatório!")
	private TabStatusObj tabStatusObj;

	@ManyToOne
	@JoinColumn(name = "cd_tipo_carregamento")
//@NotNull(message = "TipoCarregamento campo obrigatório!")
	private TabTipoCarregamentoObj  tabTipoCarregamentoObj;

	@ManyToOne
	@JoinColumn(name = "cd_destino")
//@NotNull(message = "Destino campo obrigatório!")
	private TabDestinoObj tabDestinoObj;


	@Column(name = "ck_imo_perigoso")
//@NotNull(message = "ckImoPerigoso campo obrigatório!")
	private Integer ckImoPerigoso;

	@Column(name = "ck_nao_remonte")
//@NotNull(message = "ckNaoRemonte campo obrigatório!")
	private Integer ckNaoRemonte;


	@Column(name = "tx_mercadoria")
//@NotEmpty(message = "Mercadoria campo obrigatório!")
	@Size(max = 500, message = "Mercadoria tamanho máximo de 500 caracteres")
	private String txMercadoria;


	@Column(name = "tx_lote")
//@NotEmpty(message = "Lote campo obrigatório!")
	@Size(max = 20, message = "Lote tamanho máximo de 20 caracteres")
	private String txLote;


	@Column(name = "tx_cnpj_importador")
//@NotEmpty(message = "CnpjImportador campo obrigatório!")
	@Size(max = 200, message = "CnpjImportador tamanho máximo de 200 caracteres")
	private String txCnpjImportador;

	@Column(name = "vl_peso_extrato_desova")
//@NotEmpty(message = "PesoExtratoDesova campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlPesoExtratoDesova;

	@Column(name = "vl_mercadoria_invoice")
//@NotEmpty(message = "MercadoriaInvoice campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlMercadoriaInvoice;

	@Column(name = "vl_comprimento")
//@NotEmpty(message = "Comprimento campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlComprimento;

	@Column(name = "vl_m3_pack")
//@NotEmpty(message = "M3Pack campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlM3Pack;

	@Column(name = "tx_numero_dta")
//@NotEmpty(message = "NumeroDta campo obrigatório!")
	@Size(max = 30, message = "NumeroDta tamanho máximo de 30 caracteres")
	private String txNumeroDta;


	@Column(name = "tx_bl")
//@NotEmpty(message = "Bl campo obrigatório!")
	@Size(max = 30, message = "Bl tamanho máximo de 30 caracteres")
	private String txBl;

	@Column(name = "ck_frete_exclusivo")
//@NotNull(message = "ckFreteExclusivo campo obrigatório!")
	private Integer ckFreteExclusivo;

	@Column(name = "dt_prev_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrevCarregamento;


	@Column(name = "dt_dta_solicitacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaSolicitacao;

	@Column(name = "dt_dta_registro")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaRegistro;

	@Column(name = "dt_dta_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaCarregamento;

	@Column(name = "dt_dta_parametrizacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaParametrizacao;

	@Column(name = "tx_dta_canal")
//@NotEmpty(message = "DtaCanal campo obrigatório!")
	@Size(max = 30, message = "DtaCanal tamanho máximo de 30 caracteres")
	private String txDtaCanal;

	@Column(name = "dt_dta_desembaraco")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaDesembaraco;

	@Column(name = "dt_dta_inicio_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaInicioTransito;

	@Column(name = "dt_dta_chegada_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaChegadaTransito;

	@Column(name = "dt_dta_conclusao_transito")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaConclusaoTransito;

	@Column(name = "dt_dta_ultima_consulta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaUltimaConsulta;

	@Column(name = "dt_averbacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAverbacao;

	@Column(name = "tx_email_taxa_informativo")
//@NotEmpty(message = "EmailTaxaInformativo campo obrigatório!")
	@Size(max = 300, message = "EmailTaxaInformativo tamanho máximo de 300 caracteres")
	private String txEmailTaxaInformativo;


	@Column(name = "ck_anvisa")
//@NotNull(message = "ckAnvisa campo obrigatório!")
	private Integer ckAnvisa;


	@Column(name = "vl_qtde_volume")
//@NotNull(message = "QtdeVolume campo obrigatório!")
	private Integer vlQtdeVolume;

	@Column(name = "tx_volume")
//@NotEmpty(message = "Volume campo obrigatório!")
	@Size(max = 300, message = "Volume tamanho máximo de 300 caracteres")
	private String txVolume;


	@Column(name = "tx_container")
//@NotEmpty(message = "Container campo obrigatório!")
	@Size(max = 20, message = "Container tamanho máximo de 20 caracteres")
	private String txContainer;

	@Column(name = "dt_inicio_desova")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtInicioDesova;

	@Column(name = "dt_fim_desova")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtFimDesova;

	@Column(name = "dt_saida")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtSaida;

	@Column(name = "vl_diferenca_peso")
//@NotEmpty(message = "DiferencaPeso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlDiferencaPeso;


	@Column(name = "vl_diferenca_percentual")
//@NotEmpty(message = "DiferencaPercentual campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlDiferencaPercentual;

	@Column(name = "tx_viagem")
//@NotEmpty(message = "Viagem campo obrigatório!")
	@Size(max = 20, message = "Viagem tamanho máximo de 20 caracteres")
	private String txViagem;

	@Column(name = "tx_navio")
//@NotEmpty(message = "Navio campo obrigatório!")
	@Size(max = 200, message = "Navio tamanho máximo de 200 caracteres")
	private String txNavio;


	@Column(name = "vl_peso_bruto")
//@NotEmpty(message = "PesoBruto campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlPesoBruto;


	@Column(name = "tx_situacao_ce_master")
//@NotEmpty(message = "SituacaoCeMaster campo obrigatório!")
	@Size(max = 200, message = "SituacaoCeMaster tamanho máximo de 200 caracteres")
	private String txSituacaoCeMaster;

	@ManyToOne
	@JoinColumn(name = "cd_coloader_despachante")
//@NotNull(message = "ColoaderDespachante campo obrigatório!")
	private TabClienteObj tabColoaderDespachanteObj;


	@Column(name = "dt_entrada_cd")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEntradaCd;


	@Column(name = "cd_id_mantran")
//@NotNull(message = "IdMantran campo obrigatório!")
	private Integer cdIdMantran;

	@Column(name = "tx_erro_mantran")
//@NotEmpty(message = "ErroMantran campo obrigatório!")
	@Size(max = 200, message = "ErroMantran tamanho máximo de 200 caracteres")
	private String txErroMantran;

	@Column(name = "dt_desconsolidacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDesconsolidacao;

	@Column(name = "tx_mapa")
//@NotEmpty(message = "Mapa campo obrigatório!")
	@Size(max = 3, message = "Mapa tamanho máximo de 3 caracteres")
	private String txMapa;

	@Column(name = "tx_gr_paga")
//@NotEmpty(message = "GrPaga campo obrigatório!")
	@Size(max = 3, message = "GrPaga tamanho máximo de 3 caracteres")
	private String txGrPaga;
	
	@Column(name = "tx_siscarga")
//@NotEmpty(message = "Siscarga campo obrigatório!")
	@Size(max = 3, message = "Siscarga tamanho máximo de 3 caracteres")
	private String txSiscarga;

	@Column(name = "tx_bloqueio_bl")
//@NotEmpty(message = "BloqueioBl campo obrigatório!")
	@Size(max = 3, message = "BloqueioBl tamanho máximo de 3 caracteres")
	private String txBloqueioBl;

	@Column(name = "tx_bloqueio_cntr")
//@NotEmpty(message = "BloqueioCntr campo obrigatório!")
	@Size(max = 3, message = "BloqueioCntr tamanho máximo de 3 caracteres")
	private String txBloqueioCntr;

	@Column(name = "tx_icms_sefaz")
//@NotEmpty(message = "IcmsSefaz campo obrigatório!")
	@Size(max = 3, message = "IcmsSefaz tamanho máximo de 3 caracteres")
	private String txIcmsSefaz;

	@Column(name = "ck_dta_conclusao_transito")
	private Integer ckDtaConclusaoTransito;
	
	@Column(name = "tx_cnpj")
	private String txCnpj;
	
	@Column(name = "tx_nome_pagador")
	private String txNomePagador;
	
	@Column(name = "tx_importador")
	private String txImportador;
	
	
	@OneToMany(mappedBy = "cdProposta", targetEntity = TabPropostaDocumentosObj.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<TabPropostaDocumentosObj> listaDocumentos;
	

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {

	}

}
