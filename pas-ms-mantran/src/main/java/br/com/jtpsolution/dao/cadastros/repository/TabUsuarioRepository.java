package br.com.jtpsolution.dao.cadastros.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.TabUsuarioObj;


public interface TabUsuarioRepository extends JpaRepository<TabUsuarioObj, Integer> {

	TabUsuarioObj findByTxApelido(String txApelido);

	
	TabUsuarioObj findByTxEmail(String email);
	
	TabUsuarioObj findByTxEmailAndTxSenha(String email, String senha);
	
	TabUsuarioObj findByTxEmailAndTxSenhaAndCkAtivo(String email, String senha, Integer ativo);
		
	TabUsuarioObj findByTxEmailAndCkAtivo(String txEmail, Integer ckAtivo);
	
}
