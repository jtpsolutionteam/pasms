package br.com.jtpsolution.dao.cadastros;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_destino", schema = Constants.SCHEMA)
public class TabDestinoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_destino")
	// @NotNull(message = "Destino campo obrigatório!")
	private Integer cdDestino;

	@Column(name = "tx_destino")
	// @NotEmpty(message = "Destino campo obrigatório!")
	@Size(max = 45, message = "Destino tamanho máximo de 45 caracteres")
	private String txDestino;
	
	@Column(name = "vl_dta_importacao")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDtaImportacao;
	
	@Column(name = "vl_exportacao")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlExportacao;
	
	@Column(name = "ck_ativo")
	private Integer ckAtivo;
	
	@Column(name = "ck_rota_amalog")
	private Integer ckRotaAmalog;
	
	@ManyToOne
	@JoinColumn(name = "cd_armazem")
	private TabClienteObj tabArmazemObj;
	
	@Column(name = "cd_codigo_ibge")
	private Integer cdCodigoIbge;
	
	@Column(name = "vl_icms")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlIcms;
	
	@Column(name = "vl_gris")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlGris;
	
	@Column(name = "vl_m3_minimo_truck") 
	//@NotEmpty(message = "M3MinimoTruck campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00") 
	private BigDecimal vlM3MinimoTruck; 
	 
	@Column(name = "vl_m3_minimo_carreta") 
	//@NotEmpty(message = "M3MinimoCarreta campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00") 
	private BigDecimal vlM3MinimoCarreta; 
	 
	@Column(name = "vl_peso_minimo_truck") 
	//@NotEmpty(message = "PesoMinimoTruck campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00") 
	private BigDecimal vlPesoMinimoTruck; 
	 
	@Column(name = "vl_peso_minimo_carreta") 
	//@NotEmpty(message = "PesoMinimoCarreta campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00") 
	private BigDecimal vlPesoMinimoCarreta;
	
	@Column(name = "tx_nome") 
	//@NotEmpty(message = "Nome campo obrigatório!")
	@Size(max = 150, message = "Nome tamanho máximo de 150 caracteres") 
	private String txNome; 
	 
	@Column(name = "tx_endereco") 
	//@NotEmpty(message = "Endereco campo obrigatório!")
	@Size(max = 100, message = "Endereco tamanho máximo de 100 caracteres") 
	private String txEndereco; 
	 
	@Column(name = "tx_cidade") 
	//@NotEmpty(message = "Cidade campo obrigatório!")
	@Size(max = 100, message = "Cidade tamanho máximo de 100 caracteres") 
	private String txCidade; 
	 
	@Column(name = "tx_uf") 
	//@NotEmpty(message = "Uf campo obrigatório!")
	@Size(max = 2, message = "Uf tamanho máximo de 2 caracteres") 
	private String txUf; 
	 
	@Column(name = "tx_cep") 
	//@NotEmpty(message = "Cep campo obrigatório!")
	@Size(max = 10, message = "Cep tamanho máximo de 10 caracteres") 
	private String txCep; 

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDestino))
			txDestino = txDestino.toUpperCase();
	}

	

}
